#include <iostream>
#include <memory>

#include "UHH2/core/include/AnalysisModule.h"
#include "UHH2/core/include/Event.h"
#include "UHH2/common/include/CommonModules.h"
#include "UHH2/common/include/CleaningModules.h"
#include "UHH2/common/include/ElectronHists.h"
#include "UHH2/common/include/NSelections.h"
#include "UHH2/SubstructureValidation/include/SubstructureValidationSelections.h"
#include "UHH2/SubstructureValidation/include/SubstructureValidationHists.h"
#include "UHH2/SubstructureValidation/include/SubstructureValidationECFIssueHists.h"
#include "UHH2/SubstructureValidation/include/SubstructureValidationHLTEffHists.h"
#include "UHH2/common/include/MCWeight.h"
#include "UHH2/common/include/NSelections.h"
#include "UHH2/common/include/ObjectIdUtils.h"
#include "UHH2/common/include/EventHists.h"



#include "UHH2/common/include/JetCorrections.h"
#include "UHH2/SubstructureValidation/include/TopJetCorrections.h"
#include "UHH2/common/include/YearRunSwitchers.h"
#include "UHH2/common/include/Utils.h" //mainly for runPeriods vectors
#include "UHH2/common/include/MCLargeWeightKiller.h"

#include <unistd.h>

#include <TTree.h>

#define EXTRAOUT false


using namespace std;
using namespace uhh2;

namespace uhh2examples {

  class DataPileupReweight: public uhh2::AnalysisModule {
  public:
    explicit DataPileupReweight(uhh2::Context & ctx);

    virtual bool process(uhh2::Event & event) override;

  private:
    TH1F * h_npu_data_target;
    TH1F * h_npu_data;
    map<int, map<int, float> > n_pu_map;
		
  };
  DataPileupReweight::DataPileupReweight(uhh2::Context & ctx){
    bool is_mc = ctx.get("dataset_type") == "MC";
    if(is_mc){
      cout << "Warning: DataPileupReweight should only be used if you are absolutely sure you want to do it!" << endl;
      return;
    }

    std::string pileup_directory = ctx.get("pileup_directory_data");
    std::string pileup_origin_directory = ctx.get("pileup_directory_data_origin");
    std::string pileup_tree_directory = ctx.get("pileup_tree_directory");
		
    if(pileup_directory == ""){
      throw runtime_error("DataPileupReweight: pileup_directory_data is needed!");
    }
    if(pileup_origin_directory == ""){
      throw runtime_error("DataPileupReweight: pileup_directory_data_origin is needed!");
    }

    TFile file_data_target(locate_file(pileup_directory).c_str());
    h_npu_data_target = (TH1F*) file_data_target.Get("pileup");
    h_npu_data_target->SetDirectory(0);
    h_npu_data_target->Scale(1./h_npu_data_target->Integral());

    TFile file_data(locate_file(pileup_origin_directory).c_str());
    h_npu_data = (TH1F*) file_data.Get("pileup");
    h_npu_data->SetDirectory(0);
    h_npu_data->Scale(1./h_npu_data->Integral());

    if(h_npu_data->GetNbinsX() != h_npu_data_target->GetNbinsX()){
      throw runtime_error("DataPileupReweight: pile-up histograms for data and data_target have different numbers of bins");
    }
    if( (h_npu_data->GetXaxis()->GetXmax() != h_npu_data_target->GetXaxis()->GetXmax())
        || (h_npu_data->GetXaxis()->GetXmin() != h_npu_data_target->GetXaxis()->GetXmin()))
    {
      throw runtime_error("DataPileupReweight: pile-up histograms for data and data_target have different axis ranges");
    }

    TFile pu_info_file(locate_file(pileup_tree_directory).c_str());

    TTree* pu_info_tree = (TTree*) pu_info_file.Get("pileupInfo");
    int run,lumisec;
    float n_pu;
		
    pu_info_tree->SetBranchAddress("run", &run);
    pu_info_tree->SetBranchAddress("lumisec", &lumisec);
    pu_info_tree->SetBranchAddress("n_pu", &n_pu);

    for (int i = 0, N = pu_info_tree->GetEntries(); i < N; ++i) {
      pu_info_tree->GetEntry(i);
      // std::cout << run << " " << lumisec << " " << n_pu << std::endl;			
      n_pu_map[run][lumisec]=n_pu;
    }

    // //read the map back to check if everything is good.
    // for(map<int,map<int,float>>::iterator it = n_pu_map.begin(); it != n_pu_map.end(); ++it) {			
    // 	cout << it->first <<  endl;
    // 	for(map<int,float>::iterator it1 = it->second.begin(); it1 != it->second.end(); ++it1){
    // 		cout << it1->first << " " << it1->second << endl;
    // 	}
    // }

  }


	
  bool DataPileupReweight::process(Event &event){
    double weight,N_pu = 0.0;
		
    try{
      N_pu = n_pu_map[event.run][event.luminosityBlock];
      if (N_pu < h_npu_data->GetXaxis()->GetXmin()) {
        cout << "WARNING N_pu = " << N_pu << " < lower edge of data hist = " << h_npu_data->GetXaxis()->GetXmin();
        cout << " Setting event weight_pu to 0" << endl;
        return false;
      } else if (N_pu > h_npu_data->GetXaxis()->GetXmax()) {
        cout << "WARNING N_pu = " << N_pu << " > upper edge of data hist = " << h_npu_data->GetXaxis()->GetXmax();
        return false;
      }
    }
    catch(const std::runtime_error& error){
      std::cout<<"Problem with genInfo in DataPileupReweight"<<std::endl;
      std::cout<<error.what();
    }
    int binnumber = h_npu_data->GetXaxis()->FindBin(N_pu);
    auto data_cont = h_npu_data->GetBinContent(binnumber);
    if (data_cont > 0) {
      weight = h_npu_data_target->GetBinContent(binnumber)/data_cont;
      // cout << "N_pu: "<< N_pu<< " data_cont: "<< data_cont << "  target_cont: " << h_npu_data_target->GetBinContent(binnumber)<< "  weight: " << weight <<endl;
    } else {
      cout << "WARNING no value in data hist for N_pu = " << N_pu;
      cout << " Setting event weight_pu to 0" << endl;
      return false;
    }
    event.weight *= weight;
    return true;
  }
	
	
	
  /** \brief Basic analysis example of an AnalysisModule (formerly 'cycle') in UHH2
   *
   * This is the central class which calls other AnalysisModules, Hists or Selection classes.
   * This AnalysisModule, in turn, is called (via AnalysisModuleRunner) by SFrame.
   */
  class SubstructureValidationModule: public AnalysisModule {
  public:

    explicit SubstructureValidationModule(Context & ctx);
    virtual bool process(Event & event) override;

  private:

    std::unique_ptr<CommonModules> common;
    // std::unique_ptr<DataPileupReweight> data_pu_reweight;
    std::unique_ptr<TopJetCorrections> topjetCorr;

    std::unique_ptr<JetCleaner> ak4cleaner;
    std::unique_ptr<TopJetCleaner> ak8cleaner;
    // declare the Selections to use. Use unique_ptr to ensure automatic call of delete in the destructor,
    // to avoid memory leaks.

    std::unique_ptr<Selection> N_AK8_550_sel,N_AK8_650_sel,VBF_sel;
    std::unique_ptr<MCLargeWeightKiller> mcSpikeKiller;

    std::unique_ptr<Hists> h_nocuts,h_jec,h_cleaner;
    std::unique_ptr<Hists> h_event_cleaner;
    std::unique_ptr<Hists> h_triggeff0,h_triggeff1,h_triggeff3;

    std::unique_ptr<Hists> h_nak8_550sel,h_nak8_650sel;
    std::unique_ptr<Hists> h_detaAK4sel,h_vbf_sel,h_vbf_AK8550_sel,h_vbf_AK8650_sel;

    std::unique_ptr<Hists> h_ak4_200sel;

		
    bool is_mc;
    bool PreProcessed;
    bool SaveOnlyHighCEFJetEvents;
    bool ProcessPFparticles;
    bool QCDNoPUSample;
    // bool do_data_pu_reweight;

    uhh2::Event::Handle<double> h_ht;
    uhh2::Event::Handle<double> tt_pt_ave;

    uhh2::Event::Handle<MET> h_PuppiMET;
    uhh2::Event::Handle<MET> h_PfMET;
		
    Double_t AK4_Clean_pT,AK4_Clean_eta,AK8_Clean_pT,AK8_Clean_eta;
    // Event::Handle<std::vector<Jet>> h_highCEFJets;

  };


  SubstructureValidationModule::SubstructureValidationModule(Context & ctx){

    cout << "Hello World from SubstructureValidationModule!" << endl;

    for(auto & kv : ctx.get_all()){
      cout << " " << kv.first << " = " << kv.second << endl;
    }

    is_mc = ctx.get("dataset_type") == "MC";

    PreProcessed = string2bool(ctx.get("PreProcessed", "false"));
    ProcessPFparticles = !ctx.get("PFParticleCollection","").empty();
    SaveOnlyHighCEFJetEvents = string2bool(ctx.get("SaveOnlyHighCEFJetEvents", "false"));
    if(PreProcessed)std::cout << "processing Samples that already past PreSelection!"<< std::endl;
    else std::cout<< "processing Sample that didn't pass any PreSelection!"<<std::endl;
		
    std::string version=ctx.get("dataset_version");
    QCDNoPUSample=version.find("MC_QCD_NoPU_2018_UL18_106X") != std::string::npos;
    if(QCDNoPUSample)std::cout << "QCD no PU 106X RelVal sample. Skipping hists for trigger studies"<< std::endl;
		
    common.reset(new CommonModules());
    common->disable_jetpfidfilter();
    common->disable_metfilters();
    // common->disable_jec();
    // common->disable_jersmear();
    // if(PreProcessed)common->disable_mcpileupreweight();
    common->init(ctx);

    // do_data_pu_reweight = version.find("data_pu_reweight") != std::string::npos;
    // data_pu_reweight.reset(new DataPileupReweight(ctx));

    topjetCorr.reset(new TopJetCorrections());
    topjetCorr->init(ctx);		

    AK4_Clean_pT=30.0;
    AK4_Clean_eta=5.0;
    AK8_Clean_pT=200.0;
    AK8_Clean_eta=2.5;
    ak4cleaner.reset(new JetCleaner(ctx, AK4_Clean_pT, AK4_Clean_eta));
    ak8cleaner.reset(new TopJetCleaner(ctx,TopJetId(PtEtaCut(AK8_Clean_pT,AK8_Clean_eta))));

    N_AK8_550_sel.reset(new NTopJetSelection(1,-1,TopJetId(PtEtaCut(550.,100000.))));
    N_AK8_650_sel.reset(new NTopJetSelection(1,-1,TopJetId(PtEtaCut(650.,100000.))));

    VBF_sel.reset(new VBFSelection(3.0,1000.0));

    mcSpikeKiller.reset(new MCLargeWeightKiller(ctx,
                                                2, // maximum allowed ratio of leading reco jet pT / generator HT
                                                2, // maximum allowed ratio of leading gen jet pT / generator HT
                                                2, // maximum allowed ratio of leading reco jet pT / Q scale
                                                2, // maximum allowed ratio of PU maximum pTHat / gen HT (ensures scale of PU < scale of hard interaction)
                                                2, // maximum allowed ratio of leading reco jet pT / pTHat
                                                2, // maximum allowed ratio of leading gen jet pT / pTHat
                                                "jets", // name of jet collection to be used
                                                "genjets" // name of genjet collection to be used
                          ));
		
    h_triggeff0.reset(new SubstructureValidationHLTEffHists(ctx, "TriggEff0"));
    h_nocuts.reset(new SubstructureValidationHists(ctx, "NoCuts"));
    h_triggeff1.reset(new SubstructureValidationHLTEffHists(ctx, "TriggEff1"));
    h_jec.reset(new SubstructureValidationHists(ctx, "JetCorrections"));

    h_cleaner.reset(new SubstructureValidationHists(ctx, "AfterCleaner"));
    h_event_cleaner.reset(new EventHists(ctx,"EventHistsAfterCleaner"));
    h_triggeff3.reset(new SubstructureValidationHLTEffHists(ctx, "TriggEff3"));

    h_nak8_550sel.reset(new SubstructureValidationHists(ctx, "Nak8_550sel"));
    h_nak8_650sel.reset(new SubstructureValidationHists(ctx, "Nak8_650sel"));

    h_ak4_200sel.reset(new SubstructureValidationHists(ctx, "ak4_200sel"));

    h_detaAK4sel.reset(new SubstructureValidationHists(ctx,"DetaAK4sel"));
    h_vbf_sel.reset(new SubstructureValidationHists(ctx,"VBFsel"));
    h_vbf_AK8550_sel.reset(new SubstructureValidationHists(ctx,"VBF_AK8550sel"));
    h_vbf_AK8650_sel.reset(new SubstructureValidationHists(ctx,"VBF_AK8650sel"));

    h_ht = ctx.get_handle<double>("HT");
    tt_pt_ave = ctx.declare_event_output<double>("pt_ave");

    // h_highCEFJets = ctx.declare_event_output<std::vector<Jet>>("highCEFJets");

    h_PuppiMET = ctx.declare_event_input<MET>("slimmedMETsPuppi");

  }


  bool SubstructureValidationModule::process(Event & event) {
    if(EXTRAOUT){
      cout << "SubstructureValidationModule: Starting to process event (runid, eventid) = (" << event.run << ", " << event.event << "); weight = " << event.weight << endl;
    }
    if(event.year == "2018" and !QCDNoPUSample) h_triggeff0->fill(event);
		
    // const MET & PuppiMET = event.get(h_PuppiMET); 
    // const MET & PfMET = *event.met; 
    bool pass_common=common->process(event);
    if(!pass_common) return false;

    // if(!is_mc && do_data_pu_reweight){
    // 	data_pu_reweight->process(event);
    // }
		
    topjetCorr->process(event);
		
    h_nocuts->fill(event);
		
    sort_by_pt<Jet>(*event.jets);
    sort_by_pt<TopJet>(*event.topjets);
    //End of JEC

    h_jec->fill(event);

    ak4cleaner->process(event);
    ak8cleaner->process(event);


    // Writing pTaverage to Event content
    double pTave=0.0;
    if(is_mc){
      if(event.jets->size()>0){
        pTave=event.jets->at(0).pt();
      }else	if(event.jets->size()>1){
        pTave=(event.jets->at(0).pt()+event.jets->at(1).pt())/2.0;
      }
    }
    event.set(tt_pt_ave,pTave);

    h_cleaner->fill(event);
    h_event_cleaner->fill(event);

    //Spikekiller
    if (is_mc){
      if (!mcSpikeKiller->passes(event)) return false;
    }

    if(event.year == "2018" and !QCDNoPUSample) h_triggeff3->fill(event);


    bool N_AK8_550_selection=N_AK8_550_sel->passes(event);
    bool N_AK8_650_selection=N_AK8_650_sel->passes(event);

    if(N_AK8_550_selection) h_nak8_550sel->fill(event);
    if(N_AK8_650_selection) h_nak8_650sel->fill(event);

    bool ak4_200_selection = false;
    if(event.jets->size()>1) ak4_200_selection = (event.jets->at(0).pt()>200);		
    if(ak4_200_selection) h_ak4_200sel->fill(event);


    if(event.jets->size()>1 && event.topjets->size()>1){
      //Removing AK4 Jets with deltaR(AK4,AK8_1,2)<1.2
      std::vector<Jet>* AK4Jets(new std::vector<Jet> (*event.jets));
      std::vector<TopJet> AK8Jets = *event.topjets;

      const TopJet & AK8_0 = AK8Jets[0];
      const TopJet & AK8_1 = AK8Jets[1];

      AK4Jets->clear();
      AK4Jets->reserve(event.jets->size());

      for(const Jet AK4:*event.jets){
        double deltaR_0 = deltaR(AK4,AK8_0);
        double deltaR_1 = deltaR(AK4,AK8_1);
        if(deltaR_0 > 1.2 && deltaR_1 > 1.2) AK4Jets->push_back(AK4);
      }

      sort_by_pt<Jet>(*AK4Jets);
      event.jets->clear();
      event.jets->reserve(AK4Jets->size());
      for(const auto &  jet : *AK4Jets) event.jets->push_back(jet);
      sort_by_pt<Jet>(*event.jets);
      sort_by_pt<TopJet>(*event.topjets);
		
      //checking again if two AK4 jets are present, since we possibly removed some above
      if(event.jets->size()>1){
        double Mjj_AK4 = (event.jets->at(0).v4() + event.jets->at(1).v4()).M();
        bool MjjAK4sel = Mjj_AK4>1000.0;
        double detaAK4 = abs(event.jets->at(0).eta() - event.jets->at(1).eta());
        bool detaAK4sel = detaAK4>3.0;
			
        bool VBF_selection = MjjAK4sel && detaAK4sel;
			
        if(detaAK4sel)h_detaAK4sel->fill(event);
        if(VBF_selection) h_vbf_sel->fill(event);
        if(VBF_selection && N_AK8_550_selection) h_vbf_AK8550_sel->fill(event);
        if(VBF_selection && N_AK8_650_selection) h_vbf_AK8650_sel->fill(event);
      }		
    }

		
    // std::vector<Jet> highCEFJets_vec;
    // event.set(h_highCEFJets,std::move(highCEFJets_vec));
    // if(SaveOnlyHighCEFJetEvents){
    // 	float cefThreshold = 0.6;
    // 	if(event.jets->size()<1)return false;
    // 	if(event.jets->at(0).chargedEmEnergyFraction()>cefThreshold){
    // 		highCEFJets_vec.push_back(event.jets->at(0));
    // 		event.set(h_highCEFJets,std::move(highCEFJets_vec));
    // 	}else{
    // 		return false;
    // 	}
    // }
    return true;
    // 3. decide whether or not to keep the current event in the output:
    // return njet_selection && dijet_selection;
  }

  // as we want to run the ExampleCycleNew directly with AnalysisModuleRunner,
  // make sure the SubstructureValidationModule is found by class name. This is ensured by this macro:
  UHH2_REGISTER_ANALYSIS_MODULE(SubstructureValidationModule)

}
