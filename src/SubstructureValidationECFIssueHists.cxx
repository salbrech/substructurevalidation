#include "UHH2/SubstructureValidation/include/SubstructureValidationECFIssueHists.h"
#include "UHH2/core/include/Event.h"

#include "TH1F.h"
#include "TH2F.h"
#include <iostream>

using namespace std;
using namespace uhh2;
using namespace uhh2examples;

SubstructureValidationECFIssueHists::SubstructureValidationECFIssueHists(Context & ctx, const string & dirname): Hists(ctx, dirname){

  book<TH1F>("N_PFele_AK4_1","N_{electrons} in leading AK4",20,0,20);
  book<TH1F>("CEF_AK4_1","charged EM energy fraction leading AK4",20,0,1);
  book<TH2F>("NPFele_v_CEF_AK4_1","CEF v N_{PF electrons};charged EF fraction;N_{PF electrons}",20,0,1,20,0,20);
  book<TH2F>("NPFele_v_CEF_AK4_1_CEFGT0p05","CEF v N_{PF electrons};charged EF fraction;N_{PF electrons}",20,0,1,20,0,20);

  book<TH1F>("N_PFele_AK4_2","N_{electrons} in leading AK4",20,0,20);
  book<TH1F>("CEF_AK4_2","charged EM energy fraction leading AK4",20,0,1);
  book<TH2F>("NPFele_v_CEF_AK4_2","CEF v N_{PF electrons};charged EF fraction;N_{PF electrons}",20,0,1,20,0,20);
  book<TH2F>("NPFele_v_CEF_AK4_2_CEFGT0p05","CEF v N_{PF electrons};charged EF fraction;N_{PF electrons}",20,0,1,20,0,20);
	
  book<TH1F>("N_PFele_AK4","N_{electrons} in leading AK4",20,0,20);
  book<TH1F>("CEF_AK4","charged EM energy fraction leading AK4",20,0,1);
  book<TH2F>("NPFele_v_CEF_AK4","CEF v N_{PF electrons};charged EF fraction;N_{PF electrons}",20,0,1,20,0,20);
  book<TH2F>("NPFele_v_CEF_AK4_CEFGT0p05","CEF v N_{PF electrons};charged EF fraction;N_{PF electrons}",20,0,1,20,0,20);


}


void SubstructureValidationECFIssueHists::fill(const Event & event){
  auto weight = event.weight;
  // if(event.jets->size()<1) return;
  assert(event.pfparticles);
  std::vector<PFParticle>* PFparticles = event.pfparticles;
	
  for(uint i=0;i<event.jets->size();i++){
    int N_PFele=0; 								// number of PFelectrons in jet
    for(const auto candInd : event.jets->at(i).pfcand_indexs()){
      bool isElectron = PFparticles->at(candInd).particleID()==2;
      if(isElectron)N_PFele++;
    }
    float CEF=event.jets->at(i).chargedEmEnergyFraction();
    if(i==0){
      hist("N_PFele_AK4_1")->Fill(N_PFele,weight);
      hist("CEF_AK4_1")->Fill(CEF,weight);	
      ((TH2D*)hist("NPFele_v_CEF_AK4_1"))->Fill(CEF,N_PFele,weight);	
      if(CEF>0.05)((TH2D*)hist("NPFele_v_CEF_AK4_1_CEFGT0p05"))->Fill(CEF,N_PFele,weight);
    }
    if(i==1){
      hist("N_PFele_AK4_2")->Fill(N_PFele,weight);
      hist("CEF_AK4_2")->Fill(CEF,weight);	
      ((TH2D*)hist("NPFele_v_CEF_AK4_2"))->Fill(CEF,N_PFele,weight);	
      if(CEF>0.05)((TH2D*)hist("NPFele_v_CEF_AK4_2_CEFGT0p05"))->Fill(CEF,N_PFele,weight);
    }
    hist("N_PFele_AK4")->Fill(N_PFele,weight);
    hist("CEF_AK4")->Fill(CEF,weight);	
    ((TH2D*)hist("NPFele_v_CEF_AK4"))->Fill(CEF,N_PFele,weight);	
    if(CEF>0.05)((TH2D*)hist("NPFele_v_CEF_AK4_CEFGT0p05"))->Fill(CEF,N_PFele,weight);
  }
}

SubstructureValidationECFIssueHists::~SubstructureValidationECFIssueHists(){}
