#include "UHH2/SubstructureValidation/include/SubstructureValidationSelections.h"
#include "UHH2/core/include/Event.h"

#include "TH2D.h"

#include <stdexcept>

using namespace uhh2examples;
using namespace uhh2;


DijetSelection::DijetSelection(float dphi_min_, float third_frac_max_): dphi_min(dphi_min_), third_frac_max(third_frac_max_){}
    
bool DijetSelection::passes(const Event & event){
  assert(event.jets); // if this fails, it probably means jets are not read in
  if(event.jets->size() < 2) return false;
  const auto & jet0 = event.jets->at(0);
  const auto & jet1 = event.jets->at(1);
  auto dphi = deltaPhi(jet0, jet1);
  if(dphi < dphi_min) return false;
  if(event.jets->size() == 2) return true;
  const auto & jet2 = event.jets->at(2);
  auto third_jet_frac = jet2.pt() / (0.5 * (jet0.pt() + jet1.pt()));
  return third_jet_frac < third_frac_max;
}


deltaEtaAk8Selection::deltaEtaAk8Selection(float deta_max_): deta_max(deta_max_){}

bool deltaEtaAk8Selection::passes(const Event & event){
  assert(event.topjets);
  if(event.topjets->size()<2) return false;
  auto deta = fabs(event.topjets->at(0).eta()-event.topjets->at(1).eta());
  if(deta > deta_max) return false;
  else return true;    
}

invMassAK8JetSelection::invMassAK8JetSelection(float invMass_min_): invMass_min(invMass_min_){}

bool invMassAK8JetSelection::passes(const Event & event){
  assert(event.topjets);
  if(event.topjets->size()<2) return false;
  auto invMass = (event.topjets->at(0).v4()+event.topjets->at(1).v4()).M();
  if(invMass<invMass_min){
    return false;
  }else{
    return true;
  }
}

EtaPhiCleanerSelection::EtaPhiCleanerSelection(TH2D* h_map_): h_map(h_map_){}

bool EtaPhiCleanerSelection::passes(const Event & event){
  assert(&event);

  double xMin = h_map->GetXaxis()->GetXmin();
  double xWidth = h_map->GetXaxis()->GetBinWidth(1);
 
  double yMin = h_map->GetYaxis()->GetXmin();
  double yWidth = h_map->GetYaxis()->GetBinWidth(1);
  double cutValue=0;

  const int njets = event.jets->size();
 
  for(int i=0; i < njets; i++){
    int idx_x = 0;
    int idx_y = 0;
    Jet* jet = &event.jets->at(i);// loop over all jets in event
 
    while(jet->eta() > xMin+xWidth + idx_x * xWidth) idx_x++;
    while(jet->phi() > yMin+yWidth + idx_y * yWidth) idx_y++;

    cutValue = h_map->GetBinContent(idx_x+1, idx_y+1);

    if(cutValue > 0) break;
  }

  if(cutValue > 0) return false;
  return true;
}

SingleElectronSelection::SingleElectronSelection(float pT_min_, float eta_max_, float eta_Window_min_, float eta_Window_max_): pT_min(pT_min_),eta_max(eta_max_),eta_Window_min(eta_Window_min_),eta_Window_max(eta_Window_max_){}

bool SingleElectronSelection::passes(const Event & event){
  assert(event.electrons);
  if( event.electrons->size() != 1) return false;
  double eta=abs(event.electrons->at(0).eta());
  bool etaWindow = (eta < eta_Window_min || eta > eta_Window_max ) && eta < eta_max;
  return etaWindow && event.electrons->at(0).pt() > pT_min;
}

PtAveVsQScaleSelection::PtAveVsQScaleSelection(float cutValue_): cutValue(cutValue_){}

bool PtAveVsQScaleSelection::passes(const Event & event){
  assert(&event);
  double pTave=0.0;
  if(event.jets->size()>0){
    pTave=event.jets->at(0).pt();
  }else	if(event.jets->size()>1){
    pTave=(event.jets->at(0).pt()+event.jets->at(1).pt())/2.0;		
  }
  double pThat=event.genInfo->qScale();
  double ratio=pTave/pThat;
  if(ratio < cutValue) return true;
  return false;
}

VBFSelection::VBFSelection(float deltaEta_,float Mjj_):deltaEtaMin(deltaEta_),mjjMin(Mjj_){}

bool VBFSelection::passes(const Event & event){
  assert(event.jets);
  if(event.jets->size()<2) return false;
  float mjj=(event.jets->at(0).v4()+event.jets->at(1).v4()).M();
  float deta = abs(event.jets->at(0).eta()-event.jets->at(1).eta());
  return mjj>mjjMin && deta>deltaEtaMin;	
}
