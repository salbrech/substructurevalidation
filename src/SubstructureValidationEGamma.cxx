#include <iostream>
#include <memory>

#include "UHH2/core/include/AnalysisModule.h"
#include "UHH2/core/include/Event.h"
#include "UHH2/common/include/CommonModules.h"
#include "UHH2/common/include/CleaningModules.h"
#include "UHH2/common/include/ElectronHists.h"
#include "UHH2/common/include/NSelections.h"
#include "UHH2/SubstructureValidation/include/SubstructureValidationSelections.h"
#include "UHH2/SubstructureValidation/include/SubstructureValidationHists.h"
#include "UHH2/SubstructureValidation/include/SubstructureValidationHLTEffHists.h"
#include "UHH2/common/include/NSelections.h"
#include "UHH2/common/include/ObjectIdUtils.h"

#include "UHH2/common/include/TTbarReconstruction.h"
 
#include "UHH2/common/include/JetCorrections.h"
#include "UHH2/common/include/YearRunSwitchers.h"
#include "UHH2/common/include/Utils.h" //mainly for runPeriods vectors

#include <unistd.h>


#define EXTRAOUT false


using namespace std;
using namespace uhh2;

namespace uhh2examples {

  /** \brief Basic analysis example of an AnalysisModule (formerly 'cycle') in UHH2
   * 
   * This is the central class which calls other AnalysisModules, Hists or Selection classes.
   * This AnalysisModule, in turn, is called (via AnalysisModuleRunner) by SFrame.
   */
  class SubstructureValidationEGamma: public AnalysisModule {
  public:
    
    explicit SubstructureValidationEGamma(Context & ctx);
    virtual bool process(Event & event) override;

  private:
    
    std::unique_ptr<CommonModules> common;
    
    std::unique_ptr<JetCleaner> ak4cleaner;
    std::unique_ptr<TopJetCleaner> ak8cleaner;
    // declare the Selections to use. Use unique_ptr to ensure automatic call of delete in the destructor,
    // to avoid memory leaks.
    // std::unique_ptr<Selection> njet_sel, dijet_sel;
    // std::unique_ptr<Selection> N_AK8_sel,N_AK8_600_sel,M_jj_AK8_sel,D_eta_AK8_sel;//,N_AK4_EtaPhi;

    std::unique_ptr<Selection> singleEle_sel,N_AK8_sel,N_AK4_sel,btag_1_sel,btag_2_sel,btag_3_sel,btag_1_med_sel,btag_2_med_sel,btag_3_med_sel;
		
    // store the Hists collection as member variables. Again, use unique_ptr to avoid memory leaks.
    std::unique_ptr<Hists> h_nocuts,h_mcCompMSDleading40, h_afterCommon, h_jec,h_cleaner,h_singleEle,h_NAK4,h_NAK8,h_EleAK4Sep,h_AK4AK8Sep,h_MET,h_1btag,h_2btag,h_3btag,h_1medbtag,h_2medbtag,h_3medbtag,h_WlepPt,h_WlepBTagSep;//h_njet, h_dijet, h_ele;				

    std::unique_ptr<YearSwitcher> AK4_JEC_MC,AK8_JEC_MC;
    std::unique_ptr<YearSwitcher> AK4_JEC_data,AK8_JEC_data;
    std::shared_ptr<RunSwitcher> AK4_JEC_Switcher_16,AK4_JEC_Switcher_17,AK4_JEC_Switcher_18;
    std::shared_ptr<RunSwitcher> AK8_JEC_Switcher_16,AK8_JEC_Switcher_17,AK8_JEC_Switcher_18;
		
    // std::unique_ptr<RunSwitcher> AK8_jec_switcher,AK4_jec_switcher;
    // std::unique_ptr<JetCorrector>AK4_JEC_MC;
    // std::unique_ptr<TopJetCorrector>AK8_JEC_MC;

    std::unique_ptr<JetResolutionSmearer> AK4_jet_smearer;
    std::unique_ptr<GenericJetResolutionSmearer> AK8_jet_smearer;
		
    bool is_mc;

    uhh2::Event::Handle<std::vector<Jet>> h_rawCleanedJets;
    uhh2::Event::Handle<std::vector<TopJet>> h_rawCleanedTopJets;

    Double_t AK4_Clean_pT,AK4_Clean_eta,AK8_Clean_pT,AK8_Clean_eta;
		
  };


  SubstructureValidationEGamma::SubstructureValidationEGamma(Context & ctx){
    // In the constructor, the typical tasks are to initialize the
    // member variables, in particular the AnalysisModules such as
    // CommonModules or some cleaner module, Selections and Hists.
    // But you can do more and e.g. access the configuration, as shown below.
    
    cout << "Hello World from SubstructureValidationEGamma!" << endl;
    
    // If needed, access the configuration of the module here, e.g.:
    // string testvalue = ctx.get("TestKey", "<not set>");
    // cout << "TestKey in the configuration was: " << testvalue << endl;
    
    // If running in SFrame, the keys "dataset_version", "dataset_type", "dataset_lumi",
    // and "target_lumi" are set to the according values in the xml file. For CMSSW, these are
    // not set automatically, but can be set in the python config file.
    for(auto & kv : ctx.get_all()){
      cout << " " << kv.first << " = " << kv.second << endl;
    }
    is_mc = ctx.get("dataset_type") == "MC";    
    // 1. setup other modules. CommonModules and the JetCleaner:
    common.reset(new CommonModules());
    // TODO: configure common here, e.g. by 
    // calling common->set_*_id or common->disable_*
    common->disable_jetpfidfilter();
    common->disable_metfilters();
    common->disable_jec();
    common->disable_jersmear();
		
    common->init(ctx);

    const std::string JEC_tag_2016="Summer16_07Aug2017";
    const std::string JEC_version_2016="11";
    const std::string JEC_tag_2017="Fall17_17Nov2017";
    const std::string JEC_version_2017="32";
    const std::string JEC_tag_2018="Autumn18";
    const std::string JEC_version_2018="19";
    const std::string AK4_jetcoll="AK4PFPuppi";
    const std::string AK8_jetcoll="AK8PFPuppi";		


    if(is_mc){
      AK4_JEC_MC.reset(new YearSwitcher(ctx));
      AK4_JEC_MC->setup2016(std::make_shared<JetCorrector>(ctx, JERFiles::JECFilesMC(JEC_tag_2016, JEC_version_2016, AK4_jetcoll)));
      AK4_JEC_MC->setup2017(std::make_shared<JetCorrector>(ctx, JERFiles::JECFilesMC(JEC_tag_2017, JEC_version_2017, AK4_jetcoll)));
      AK4_JEC_MC->setup2018(std::make_shared<JetCorrector>(ctx, JERFiles::JECFilesMC(JEC_tag_2018, JEC_version_2018, AK4_jetcoll)));

      AK8_JEC_MC.reset(new YearSwitcher(ctx));
      AK8_JEC_MC->setup2016(std::make_shared<TopJetCorrector>(ctx, JERFiles::JECFilesMC(JEC_tag_2016, JEC_version_2016, AK8_jetcoll)));
      AK8_JEC_MC->setup2017(std::make_shared<TopJetCorrector>(ctx, JERFiles::JECFilesMC(JEC_tag_2017, JEC_version_2017, AK8_jetcoll)));
      AK8_JEC_MC->setup2018(std::make_shared<TopJetCorrector>(ctx, JERFiles::JECFilesMC(JEC_tag_2018, JEC_version_2018, AK8_jetcoll)));

      // AK4_JEC_MC.reset(new JetCorrector(ctx,JERFiles::JECFilesMC(JEC_tag,JEC_version,AK4_jetcoll)));
      // AK8_JEC_MC.reset(new TopJetCorrector(ctx,JERFiles::JECFilesMC(JEC_tag,JEC_version,AK8_jetcoll)));

      // const JERSmearing::SFType1 AK8_JER_sf=JERSmearing::SF_13TeV_Autumn18_RunABCD_V4;
      // const TString resFilename="2018/Autumn18_V4_MC_PtResolution_AK8PFPuppi.txt";		
      // AK4_jet_smearer.reset(new JetResolutionSmearer(ctx));
      // if(EXTRAOUT)std::cout << "AK4jetER_smearer set up!" << std::endl;
      // AK8_jet_smearer.reset(new GenericJetResolutionSmearer(ctx, "topjets","gentopjets", & AK8_JER_sf, resFilename));
      // if(EXTRAOUT)std::cout << "AK8jetER_smearer set up!" << std::endl;
    }else{
      AK4_JEC_Switcher_16.reset(new RunSwitcher(ctx, "2016"));
      for (const auto & runItr : runPeriods2016) { // runPeriods defined in common/include/Utils.h
        AK4_JEC_Switcher_16->setupRun(runItr, std::make_shared<JetCorrector>(ctx, JERFiles::JECFilesDATA(JEC_tag_2016, JEC_version_2016, AK4_jetcoll, runItr)));
      }

      AK4_JEC_Switcher_17.reset(new RunSwitcher(ctx, "2017"));
      for (const auto & runItr : runPeriods2017) {
        AK4_JEC_Switcher_17->setupRun(runItr, std::make_shared<JetCorrector>(ctx, JERFiles::JECFilesDATA(JEC_tag_2017, JEC_version_2017, AK4_jetcoll, runItr)));
      }

      AK4_JEC_Switcher_18.reset(new RunSwitcher(ctx, "2018"));
      for (const auto & runItr : runPeriods2018) {
        AK4_JEC_Switcher_18->setupRun(runItr, std::make_shared<JetCorrector>(ctx, JERFiles::JECFilesDATA(JEC_tag_2018, JEC_version_2018, AK4_jetcoll, runItr)));
      }

      AK4_JEC_data.reset(new YearSwitcher(ctx));
      AK4_JEC_data->setup2016(AK4_JEC_Switcher_16);
      AK4_JEC_data->setup2017(AK4_JEC_Switcher_17);
      AK4_JEC_data->setup2018(AK4_JEC_Switcher_18);

      AK8_JEC_Switcher_16.reset(new RunSwitcher(ctx, "2016"));
      for (const auto & runItr : runPeriods2016) { // runPeriods defined in common/include/Utils.h
        AK8_JEC_Switcher_16->setupRun(runItr, std::make_shared<TopJetCorrector>(ctx, JERFiles::JECFilesDATA(JEC_tag_2016, JEC_version_2016, AK8_jetcoll, runItr)));
      }

      AK8_JEC_Switcher_17.reset(new RunSwitcher(ctx, "2017"));
      for (const auto & runItr : runPeriods2017) {
        AK8_JEC_Switcher_17->setupRun(runItr, std::make_shared<TopJetCorrector>(ctx, JERFiles::JECFilesDATA(JEC_tag_2017, JEC_version_2017, AK8_jetcoll, runItr)));
      }

      AK8_JEC_Switcher_18.reset(new RunSwitcher(ctx, "2018"));
      for (const auto & runItr : runPeriods2018) {
        AK8_JEC_Switcher_18->setupRun(runItr, std::make_shared<TopJetCorrector>(ctx, JERFiles::JECFilesDATA(JEC_tag_2018, JEC_version_2018, AK8_jetcoll, runItr)));
      }

      AK8_JEC_data.reset(new YearSwitcher(ctx));
      AK8_JEC_data->setup2016(AK8_JEC_Switcher_16);
      AK8_JEC_data->setup2017(AK8_JEC_Switcher_17);
      AK8_JEC_data->setup2018(AK8_JEC_Switcher_18);

      // AK4_jec_switcher.reset(new RunSwitcher(ctx,"2018"));
      // for (const auto & runItr : runPeriods2018){
      // 	AK4_jec_switcher->setupRun(runItr,std::make_shared<JetCorrector>(ctx,JERFiles::JECFilesDATA(JEC_tag,JEC_version,AK4_jetcoll,runItr)));
      // }

      // AK8_jec_switcher.reset(new RunSwitcher(ctx,"2018"));
      // for (const auto & runItr : runPeriods2018){
      // 	AK8_jec_switcher->setupRun(runItr,std::make_shared<TopJetCorrector>(ctx,JERFiles::JECFilesDATA(JEC_tag,JEC_version,AK8_jetcoll,runItr)));
      // }
    }
		
    AK4_Clean_pT=30.0;
    AK4_Clean_eta=5.0;
    AK8_Clean_pT=200.0;
    AK8_Clean_eta=2.5;
    ak4cleaner.reset(new JetCleaner(ctx, AK4_Clean_pT, AK4_Clean_eta)); 
    ak8cleaner.reset(new TopJetCleaner(ctx,TopJetId(PtEtaCut(AK8_Clean_pT,AK8_Clean_eta))));
	    
    // note that the JetCleaner is only kept for the sake of example;
    // instead of constructing a jetcleaner explicitly,
    // the cleaning can also be achieved with less code via CommonModules with:
    // common->set_jet_id(PtEtaCut(30.0, 2.4));
    // before the 'common->init(ctx)' line.
    
    // 2. set up selections
    // njet_sel.reset(new NJetSelection(2)); // see common/include/NSelections.h
    // dijet_sel.reset(new DijetSelection()); // see SubstructureValidationSelections

    singleEle_sel.reset(new SingleElectronSelection(120, 2.5,1.442,1.56));
    N_AK4_sel.reset(new NJetSelection(1,-1,JetId(PtEtaCut(30.,2.4))));
    N_AK8_sel.reset(new NTopJetSelection(1,-1,TopJetId(PtEtaCut(200.,2.5))));

    // BTag(algo,wp)
    // enum algo {CSVV2, DEEPCSV, DEEPJET};
    // enum wp {WP_LOOSE  = 0,WP_MEDIUM = 1,WP_TIGHT = 2 };
    btag_1_sel.reset(new NJetSelection(1,-1,JetId(BTag(BTag::CSVV2,BTag::WP_TIGHT))));
    btag_2_sel.reset(new NJetSelection(2,-1,JetId(BTag(BTag::CSVV2,BTag::WP_TIGHT))));
    btag_3_sel.reset(new NJetSelection(3,-1,JetId(BTag(BTag::CSVV2,BTag::WP_TIGHT))));
    btag_1_med_sel.reset(new NJetSelection(1,-1,JetId(BTag(BTag::CSVV2,BTag::WP_MEDIUM))));
    btag_2_med_sel.reset(new NJetSelection(2,-1,JetId(BTag(BTag::CSVV2,BTag::WP_MEDIUM))));
    btag_3_med_sel.reset(new NJetSelection(3,-1,JetId(BTag(BTag::CSVV2,BTag::WP_MEDIUM))));

		
    // N_AK4_EtaPhi.reset(new NJetSelection(1,-1,JetId(JetEtaPhiCleaningId(h_map))));
    // N_AK8_sel.reset(new NTopJetSelection(1,-1,TopJetId(PtEtaCut(500.,100000.))));
    // N_AK8_600_sel.reset(new NTopJetSelection(1,-1,TopJetId(PtEtaCut(600.,100000.))));
    // N_AK8_sel.reset(new NTopJetSelection(2));
    // M_jj_AK8_sel.reset(new invMassAK8JetSelection(1050.0f));
    // D_eta_AK8_sel.reset(new deltaEtaAk8Selection(1.3f));
		
    // 3. Set up Hists classes:
    // h_triggeff0.reset(new SubstructureValidationHLTEffHists(ctx, "TriggEff0"));
    h_nocuts.reset(new SubstructureValidationHists(ctx, "NoCuts"));
    // h_triggeff1.reset(new SubstructureValidationHLTEffHists(ctx, "TriggEff1"));
    h_jec.reset(new SubstructureValidationHists(ctx, "JetCorrections"));
    h_mcCompMSDleading40.reset(new SubstructureValidationHists(ctx,"McCompMSD40"));
    h_cleaner.reset(new SubstructureValidationHists(ctx, "AfterCleaner"));
    h_singleEle.reset(new SubstructureValidationHists(ctx,"SingleEleSel"));
    h_EleAK4Sep.reset(new SubstructureValidationHists(ctx,"EleAK4Sep"));
    h_NAK8.reset(new SubstructureValidationHists(ctx,"NAK8Sel"));
    h_AK4AK8Sep.reset(new SubstructureValidationHists(ctx,"AK4AK8Sep"));
    h_NAK4.reset(new SubstructureValidationHists(ctx,"NAK4Sel"));
    h_MET.reset(new SubstructureValidationHists(ctx,"METSel"));
    h_1btag.reset(new SubstructureValidationHists(ctx,"1bTagSel"));
    h_2btag.reset(new SubstructureValidationHists(ctx,"2bTagSel"));
    h_3btag.reset(new SubstructureValidationHists(ctx,"3bTagSel"));
    h_1medbtag.reset(new SubstructureValidationHists(ctx,"1medbTagSel"));
    h_2medbtag.reset(new SubstructureValidationHists(ctx,"2medbTagSel"));
    h_3medbtag.reset(new SubstructureValidationHists(ctx,"3medbTagSel"));
    h_WlepPt.reset(new SubstructureValidationHists(ctx,"WlepPt"));
    h_WlepBTagSep.reset(new SubstructureValidationHists(ctx,"WlepBTagSep"));
    // h_nak8sel.reset(new SubstructureValidationHists(ctx, "Nak8sel"));
    // h_nak8600sel.reset(new SubstructureValidationHists(ctx, "Nak8600sel"));
    // h_mjjak8sel.reset(new SubstructureValidationHists(ctx, "Mjjak8sel"));
    // h_detak8sel.reset(new SubstructureValidationHists(ctx, "Detak8sel"));
    // h_njet.reset(new SubstructureValidationHists(ctx, "Njet"));
    // h_dijet.reset(new SubstructureValidationHists(ctx, "Dijet"));
    // h_ele.reset(new ElectronHists(ctx, "ele_nocuts"));
    h_rawCleanedJets = ctx.declare_event_output<vector<Jet>>("rawCleanedJets");
    h_rawCleanedTopJets = ctx.declare_event_output<vector<TopJet>>("rawCleanedTopJets");
  }


  bool SubstructureValidationEGamma::process(Event & event) {
    // This is the main procedure, called for each event. Typically,
    // do some pre-processing by calling the modules' process method
    // of the modules constructed in the constructor (1).
    // Then, test whether the event passes some selection and -- if yes --
    // use it to fill the histograms (2).
    // Finally, decide whether or not to keep the event in the output (3);
    // this is controlled by the return value of this method: If it
    // returns true, the event is kept; if it returns false, the event
    // is thrown away.
    
    if(EXTRAOUT){
      cout << "SubstructureValidationEGamma: Starting to process event (runid, eventid) = (" << event.run << ", " << event.event << "); weight = " << event.weight << endl;
    }
    // 1. run all modules other modules.
    bool pass_common=common->process(event);		
    if(!pass_common) return false;		
    h_nocuts->fill(event);		
    
    std::vector<Jet> rawCleanedJets;
    for(const Jet &jet : *event.jets){
      if(jet.pt()*jet.JEC_factor_raw() > AK4_Clean_pT && fabs(jet.eta() < AK4_Clean_eta)){
        rawCleanedJets.push_back(jet);
      }
    }		
    sort_by_pt<Jet>(rawCleanedJets);
    event.set(h_rawCleanedJets,std::move(rawCleanedJets));

    std::vector<TopJet> rawCleanedTopJets;
    for(const TopJet &jet : *event.topjets){
      if(jet.pt()*jet.JEC_factor_raw() > AK8_Clean_pT && fabs(jet.eta() < AK8_Clean_eta)){
        rawCleanedTopJets.push_back(jet);
      }
    }		
    sort_by_pt<TopJet>(rawCleanedTopJets);
    event.set(h_rawCleanedTopJets,std::move(rawCleanedTopJets));

		
    if(is_mc){
      AK4_JEC_MC->process(event);
      AK8_JEC_MC->process(event);
      // AK4_jet_smearer->process(event);
      // AK8_jet_smearer->process(event);
    }else{
      AK8_JEC_data->process(event);
      AK4_JEC_data->process(event);
    }
    sort_by_pt<Jet>(*event.jets);
    sort_by_pt<TopJet>(*event.topjets);

    h_jec->fill(event);		

    if(event.topjets->size()>0){
      if(event.topjets->at(0).softdropmass() > 40.)  h_mcCompMSDleading40->fill(event);
    }		
    ak4cleaner->process(event);
    ak8cleaner->process(event);

    h_cleaner->fill(event);

		
    bool SingleEle_selection = singleEle_sel->passes(event);
    if(!SingleEle_selection) return false;
    h_singleEle->fill(event);

    //BEGIN: Removing AK4 Jets with deltaR(AK4,electron)<0.3
    std::vector<Jet>* AK4Jets(new std::vector<Jet> (*event.jets));

    const Electron & ele = event.electrons->at(0);

    AK4Jets->clear();
    AK4Jets->reserve(event.jets->size());

    for(const Jet AK4:*event.jets){
      double deltaR_ele = deltaR(AK4,ele);
      if(deltaR_ele > 0.3) AK4Jets->push_back(AK4);
    }

    sort_by_pt<Jet>(*AK4Jets);
    event.jets->clear();
    event.jets->reserve(AK4Jets->size());
    for(const auto &  jet : *AK4Jets) event.jets->push_back(jet);
    sort_by_pt<Jet>(*event.jets);
    //END: Removal of AK4 (DR(AK4,electron))

    h_EleAK4Sep->fill(event);

    bool NAK8_selection = N_AK8_sel->passes(event);
    if(!NAK8_selection) return false;
    h_NAK8->fill(event);
		

    //BEGIN: Removing AK8 Jets with deltaR(AK4,AK8_1,2)<0.8
    std::vector<Jet>* AK4Jets1(new std::vector<Jet> (*event.jets));
 
    const TopJet & AK8 = event.topjets->at(0);

    AK4Jets1->clear();
    AK4Jets1->reserve(event.jets->size());

    for(const Jet AK4:*event.jets){
      double deltaR_AK8 = deltaR(AK4,AK8);
      if(deltaR_AK8 > 0.8) AK4Jets1->push_back(AK4);
    }

    sort_by_pt<Jet>(*AK4Jets1);
    event.jets->clear();
    event.jets->reserve(AK4Jets1->size());
    for(const auto &  jet : *AK4Jets1) event.jets->push_back(jet);
    sort_by_pt<Jet>(*event.jets);
    //END: Removal of AK4 (DR(AK4,AK8))
    h_AK4AK8Sep->fill(event);

    bool NAK4_selection = N_AK4_sel->passes(event);
    if(!NAK4_selection) return false;
    h_NAK4->fill(event);
    bool MET_selection=(event.met->pt() >= 80.);
    if(!MET_selection) return false;
    h_MET->fill(event);

    bool OneBtagSel=btag_1_sel->passes(event);
    if(!OneBtagSel) return false;
    h_1btag->fill(event);

		
    // bool TwoBtagSel=btag_2_sel->passes(event);
    // if(!TwoBtagSel) h_2btag->fill(event);
    // bool ThreeBtagSel=btag_3_sel->passes(event);
    // if(!ThreeBtagSel) h_3btag->fill(event);

    // bool OneMedBtagSel=btag_1_med_sel->passes(event);
    // if(!OneMedBtagSel) h_1medbtag->fill(event);
    // bool TwoMedBtagSel=btag_2_med_sel->passes(event);
    // if(!TwoMedBtagSel) h_2medbtag->fill(event);
    // bool ThreeMedBtagSel=btag_3_med_sel->passes(event);
    // if(!ThreeMedBtagSel) h_3medbtag->fill(event);


    std::vector<LorentzVector> neutrinos=NeutrinoReconstruction(event.electrons->at(0).v4(),event.met->v4());
    float maxWlepPt=-1;
    LorentzVector WlepCand;
    for(unsigned int i=0; i<neutrinos.size();i++){
      LorentzVector Wlep=event.electrons->at(0).v4()+neutrinos[i];
      if(Wlep.pt()>maxWlepPt){
        maxWlepPt=Wlep.pt();
        WlepCand=Wlep;
      }
    }
    if(WlepCand.pt()<200) return false;
		
    h_WlepPt->fill(event);

    JetId btag=JetId(BTag(BTag::CSVV2,BTag::WP_TIGHT));
    float smallestDR=10000;
		
    for(const auto & jet : *event.jets){
      if(!btag(jet,event)) continue;
      float newDR=deltaR(WlepCand,jet.v4());
      if(smallestDR>newDR) smallestDR=newDR;
    }
    if(smallestDR>2.0) return false;
    h_WlepBTagSep->fill(event);


    // bool N_AK8_selection=N_AK8_sel->passes(event);
    // if(!N_AK8_selection) return false;		
    // h_nak8sel->fill(event);


    // 2. test selections and fill histograms
    // h_ele->fill(event);
    
    // bool njet_selection = njet_sel->passes(event);
    // if(njet_selection){
    // 	h_njet->fill(event);
    // }
    return true;
    // 3. decide whether or not to keep the current event in the output:
    // return njet_selection && dijet_selection;
  }

  // as we want to run the ExampleCycleNew directly with AnalysisModuleRunner,
  // make sure the SubstructureValidationEGamma is found by class name. This is ensured by this macro:
  UHH2_REGISTER_ANALYSIS_MODULE(SubstructureValidationEGamma)

}
