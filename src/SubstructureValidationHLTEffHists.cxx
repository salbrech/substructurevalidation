#include "UHH2/SubstructureValidation/include/SubstructureValidationHLTEffHists.h"
#include "UHH2/core/include/Event.h"

#include "TH1F.h"
#include "TH2F.h"
#include <iostream>

using namespace std;
using namespace uhh2;
using namespace uhh2examples;

SubstructureValidationHLTEffHists::SubstructureValidationHLTEffHists(Context & ctx, const string & dirname): Hists(ctx, dirname){
  // book all histograms here
  // book<TH1F>("PFJET_PT", "p_{T}^{PFJet,/JETHT/} [GeV/c]", 40 ,0 ,3000);
  // book<TH1F>("PFJET_PT_450", "p_{T}^{PFJet,hltSinglePFJet450} [GeV/c]", 40 ,0 ,3000);
  // book<TH1F>("PFJET_PT_500_450", "p_{T}^{PFJet,hltSinglePFJet500|hltSinglePFJet450} [GeV/c]", 40 ,0 ,3000);
  book<TH1F>("AK4_PT", "p_{T}^{PFJet,/JETHT/} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK4_PT_320", "p_{T}^{PFJet,hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK4_PT_450_320", "p_{T}^{PFJet,hltSinglePFJet450|hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK4_PT_500_320", "p_{T}^{PFJet,hltSinglePFJet500|hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK4_PT_550_320", "p_{T}^{PFJet,hltSinglePFJet550|hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK4_PT_450", "p_{T}^{PFJet,hltSinglePFJet450} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK4_PT_500_450", "p_{T}^{PFJet,hltSinglePFJet500|hltSinglePFJet450} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK4_PT_500", "p_{T}^{PFJet,hltSinglePFJet500} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK4_PT_550_500", "p_{T}^{PFJet,hltSinglePFJet550|hltSinglePFJet500} [GeV/c]", 300 ,0 ,3000);

	
  book<TH1F>("AK8_PT", "p_{T}^{PFJet,/JETHT/} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK8_PT_320", "p_{T}^{PFJet,hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK8_PT_450_320", "p_{T}^{PFJet,hltSinglePFJet450|hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK8_PT_500_320", "p_{T}^{PFJet,hltSinglePFJet500|hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK8_PT_550_320", "p_{T}^{PFJet,hltSinglePFJet550|hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK8_PT_450", "p_{T}^{PFJet,hltSinglePFJet450} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK8_PT_500_450", "p_{T}^{PFJet,hltSinglePFJet500|hltSinglePFJet450} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK8_PT_500", "p_{T}^{PFJet,hltSinglePFJet500} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("AK8_PT_550_500", "p_{T}^{PFJet,hltSinglePFJet550|hltSinglePFJet500} [GeV/c]", 300 ,0 ,3000);

  book<TH1F>("HT", "H_{T}^{PFJets,/JETHT/} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("HT_320", "H_{T}^{PFJets,hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("HT_450_320", "H_{T}^{PFJets,hltSinglePFJet450|hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("HT_500_320", "H_{T}^{PFJets,hltSinglePFJet500|hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("HT_550_320", "H_{T}^{PFJets,hltSinglePFJet550|hltSinglePFJet320} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("HT_450", "H_{T}^{PFJets,hltSinglePFJet450} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("HT_500_450", "H_{T}^{PFJets,hltSinglePFJet500|hltSinglePFJet450} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("HT_500", "H_{T}^{PFJets,hltSinglePFJet500} [GeV/c]", 300 ,0 ,3000);
  book<TH1F>("HT_550_500", "H_{T}^{PFJets,hltSinglePFJet550|hltSinglePFJet500} [GeV/c]", 300 ,0 ,3000);


  // "triggerObjects_hltSinglePFJet500"
  // "triggerObjects_hltSinglePFJet450"
  //handle_trigger220_HF = ctx.declare_event_input< vector< FlavorParticle > >("triggerObjects_hltDiPFJetAve220ForHFJEC" );
  // h_lowTrObjects = ctx.declare_event_input<vector<FlavorParticle>>("triggerObjects_hltSinglePFJet450");
  // h_highTrObjects = ctx.declare_event_input<vector<FlavorParticle>>("triggerObjects_hltSinglePFJet500");
  h_ht = ctx.get_handle<double>("HT");
}


void SubstructureValidationHLTEffHists::fill(const Event & event){
  // fill the histograms. Please note the comments in the header file:
  // 'hist' is used here a lot for simplicity, but it will be rather
  // slow when you have many histograms; therefore, better
  // use histogram pointers as members as in 'UHH2/common/include/ElectronHLTEffHists.h'
  
  // Don't forget to always use the weight when filling.
  // HLT_PFJet320_v20
  // HLT_PFJet400_v20
  // HLT_PFJet450_v21
  // HLT_PFJet500_v21
  // HLT_PFJet550_v11
	
  // const vector<FlavorParticle> & lowTrObjs = event.get(h_lowTrObjects); 
  // const vector<FlavorParticle> & highTrObjs = event.get(h_highTrObjects); 
  auto PFJET320 = event.get_trigger_index("HLT_PFJet320_v*");
  auto PFJET450 = event.get_trigger_index("HLT_PFJet450_v*");
  auto PFJET500 = event.get_trigger_index("HLT_PFJet500_v*");		
  auto PFJET550 = event.get_trigger_index("HLT_PFJet550_v*");		

  float ht=-1.0f;
  if(event.is_valid(h_ht)){
    ht = event.get(h_ht);
    hist("HT")->Fill(ht,1);
    //EffPFJET450
    if (event.passes_trigger(PFJET320)) {
      hist("HT_320")->Fill(ht,1);
      if (event.passes_trigger(PFJET450)) {
        hist("HT_450_320")->Fill(ht,1);
      }		
      if (event.passes_trigger(PFJET500)) {
        hist("HT_500_320")->Fill(ht,1);
      }		
      if (event.passes_trigger(PFJET550)) {
        hist("HT_550_320")->Fill(ht,1);
      }		
    }
    //EffPFJET500
    if (event.passes_trigger(PFJET450)) {
      hist("HT_450")->Fill(ht,1);
      if (event.passes_trigger(PFJET500)) {
        hist("HT_500_450")->Fill(ht,1);
      }		
    }
    //EffPFJET550
    if (event.passes_trigger(PFJET500)) {
      hist("HT_500")->Fill(ht,1);
      if (event.passes_trigger(PFJET550)) {
        hist("HT_550_500")->Fill(ht,1);
      }		
    }
		
  }
  if(event.topjets->size()>0){
    const TopJet & AK8_0 = event.topjets->at(0);
    hist("AK8_PT")->Fill(AK8_0.pt(),1);
    //EffPFJET450
    if (event.passes_trigger(PFJET320)) {
      hist("AK8_PT_320")->Fill(AK8_0.pt(),1);
      if (event.passes_trigger(PFJET450)) {
        hist("AK8_PT_450_320")->Fill(AK8_0.pt(),1);
      }		
      if (event.passes_trigger(PFJET500)) {
        hist("AK8_PT_500_320")->Fill(AK8_0.pt(),1);
      }		
      if (event.passes_trigger(PFJET550)) {
        hist("AK8_PT_550_320")->Fill(AK8_0.pt(),1);
      }		
    }
    //EffPFJET500
    if (event.passes_trigger(PFJET450)) {
      hist("AK8_PT_450")->Fill(AK8_0.pt(),1);
      if (event.passes_trigger(PFJET500)) {
        hist("AK8_PT_500_450")->Fill(AK8_0.pt(),1);
      }		
    }
    //EffPFJET550
    if (event.passes_trigger(PFJET500)) {
      hist("AK8_PT_500")->Fill(AK8_0.pt(),1);
      if (event.passes_trigger(PFJET550)) {
        hist("AK8_PT_550_500")->Fill(AK8_0.pt(),1);
      }		
    }
  }

  if(event.jets->size()>0){
    const Jet & AK4_0 = event.jets->at(0);
    hist("AK4_PT")->Fill(AK4_0.pt(),1);
    //EffPFJET450
    if (event.passes_trigger(PFJET320)) {
      hist("AK4_PT_320")->Fill(AK4_0.pt(),1);
      if (event.passes_trigger(PFJET450)) {
        hist("AK4_PT_450_320")->Fill(AK4_0.pt(),1);
      }		
      if (event.passes_trigger(PFJET500)) {
        hist("AK4_PT_500_320")->Fill(AK4_0.pt(),1);
      }		
      if (event.passes_trigger(PFJET550)) {
        hist("AK4_PT_550_320")->Fill(AK4_0.pt(),1);
      }		
    }
    //EffPFJET500
    if (event.passes_trigger(PFJET450)) {
      hist("AK4_PT_450")->Fill(AK4_0.pt(),1);
      if (event.passes_trigger(PFJET500)) {
        hist("AK4_PT_500_450")->Fill(AK4_0.pt(),1);
      }		
    }
    //EffPFJET550
    if (event.passes_trigger(PFJET500)) {
      hist("AK4_PT_500")->Fill(AK4_0.pt(),1);
      if (event.passes_trigger(PFJET550)) {
        hist("AK4_PT_550_500")->Fill(AK4_0.pt(),1);
      }		
    }
  }
}

SubstructureValidationHLTEffHists::~SubstructureValidationHLTEffHists(){}
