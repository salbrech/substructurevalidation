#include "UHH2/SubstructureValidation/include/SubstructureValidationHists.h"
#include "UHH2/core/include/Event.h"
#include "UHH2/common/include/JetIds.h"
#include "UHH2/common/include/TTbarReconstruction.h"

#include "TH1F.h"
#include "TH2F.h"
#include <iostream>

using namespace std;
using namespace uhh2;
using namespace uhh2examples;

std::string getEtaBinSuffix(float etaMin,float etaMax,const char* var="eta");

SubstructureValidationHists::SubstructureValidationHists(Context & ctx, const string & dirname): Hists(ctx, dirname){
  // book all histograms here

  // primary vertices
  book<TH1F>("N_pv", "N^{PV}", 120, 0, 120);
  book<TH1F>("N_AK4", "N_{AK4 jets}", 20, 0, 20);  
  book<TH1F>("N_AK8", "N_{AK8 jets}", 20, 0, 20);

 
  // std::map<const char*, std::vector<std::string> > histNameSuffix;

  histNameSuffix['a']={"","all AK8 Jets","all AK4 Jets"};
  histNameSuffix['f']={"_1","1st AK8 Jet","1st AK4 Jet"};
  histNameSuffix['s']={"_2","2nd AK8 Jet","2nd AK4 Jet"};
  histNameSuffix['l']={"_12","leading AK8 Jets","leading AK4 Jets"};

  Selections={0,500,550,600,650,700};
  //
  //START ETABINS
  //  0   1   2   3   4   5
  // 0-1.3-2.5-2.8-3.0-5.0-Inf
	
  for(map<char,std::vector<std::string>>::iterator it = histNameSuffix.begin(); it != histNameSuffix.end(); ++it) {

    // cout << "Setting up histograms with following suffixes:" << endl;
    // cout << it->first << "::{"<<it->second[0]<<","<<it->second[1]<<"}" << endl;
		
    book<TH1F>("pT_AK4"+it->second[0], "p_{T}^{"+it->second[2]+"} [GeV/c]", 40 ,0 ,3000);
    book<TH1F>("pT_AK8"+it->second[0], "p_{T}^{"+it->second[1]+"} [GeV/c]", 40 ,0 ,3000);
    book<TH1F>("pT_raw_AK4"+it->second[0], "p_{T}^{raw, "+it->second[2]+"} [GeV/c]", 40 ,0 ,3000);
    book<TH1F>("pT_raw_AK8"+it->second[0], "p_{T}^{raw, "+it->second[1]+"} [GeV/c]", 40 ,0 ,3000);
    book<TH1F>("eta_AK4"+it->second[0], "#eta^{"+it->second[2]+"}", 40, -6.5, 6.5);
    book<TH1F>("eta_AK8"+it->second[0], "#eta^{"+it->second[1]+"}", 40, -6.5, 6.5);
    book<TH2F>("eta_phi_AK4"+it->second[0], "#eta v. phi "+it->second[2],100,-4.8,4.8,144,-3.15,3.15);
		
		
    book<TH1F>("E_AK4"+it->second[0],"E_{"+it->second[2]+"} [GeV]",100,0,5000);
    book<TH1F>("E_AK8"+it->second[0],"E_{"+it->second[1]+"} [GeV]",100,0,5000);
		
    book<TH1F>("M_AK4"+it->second[0],"M_{"+it->second[2]+"} [GeV/c^{2}]",100,0,1000);
    book<TH1F>("M_AK8"+it->second[0],"M_{"+it->second[1]+"} [GeV/c^{2}]",100,0,1000);
    book<TH1F>("M_softdrop"+it->second[0],"M_{softdrop, "+it->second[1]+"} [GeV/c^{2}]",100,0,1000);
		
    //pT vs pTraw selections
    // for(float & PtCut : Selections){
    // 	std::stringstream stream;
    // 	stream << std::fixed << std::setprecision(0) << PtCut;
    // 	std::string PtCut_s = stream.str();
    // 	book<TH1F>("pT_AK4"+it->second[0]+"_"+PtCut_s+"GEV","p_{T}^{"+it->second[2]+"} [GeV/c]",300,0,3000);
    // 	book<TH1F>("pT_AK4"+it->second[0]+"_raw_"+PtCut_s+"GEV","p_{T}^{"+it->second[2]+"} [GeV/c]",300,0,3000);
    // 	book<TH1F>("pT_AK8"+it->second[0]+"_"+PtCut_s+"GEV","p_{T}^{"+it->second[1]+"} [GeV/c]",300,0,3000);
    // 	book<TH1F>("pT_AK8"+it->second[0]+"_raw_"+PtCut_s+"GEV","p_{T}^{"+it->second[1]+"} [GeV/c]",300,0,3000);
    // }
		
    //substructure
    book<TH1F>("N_daughters_AK4"+it->second[0],"N_{daughters}^{"+it->second[2]+"}",100,0,100);
    book<TH1F>("N_daughters_AK8"+it->second[0],"N_{daughters}^{"+it->second[1]+"}",100,0,100);
		
    for(uint i=0; i<=etaBins.size()-2; i++){
      float etaMin=etaBins[i];
      float etaMax=etaBins[i+1];
      std::string etaBinSuffix = getEtaBinSuffix(etaMin,etaMax);
		
      book<TH1F>("CHF_AK4"+it->second[0]+etaBinSuffix,"charged hadron fraction^{"+it->second[2]+"}",100,0,1);
      book<TH1F>("NHF_AK4"+it->second[0]+etaBinSuffix,"neutral hadron fraction^{"+it->second[2]+"}",100,0,1);
      book<TH1F>("CEF_AK4"+it->second[0]+etaBinSuffix,"charged em fraction^{"+it->second[2]+"}",100,0,1);
      book<TH1F>("NEF_AK4"+it->second[0]+etaBinSuffix,"neutral em fraction^{"+it->second[2]+"}",100,0,1);
			
      book<TH1F>("CHF_AK8"+it->second[0]+etaBinSuffix,"charged hadron fraction^{"+it->second[1]+"}",100,0,1);
      book<TH1F>("NHF_AK8"+it->second[0]+etaBinSuffix,"neutral hadron fraction^{"+it->second[1]+"}",100,0,1);
      book<TH1F>("CEF_AK8"+it->second[0]+etaBinSuffix,"charged em fraction^{"+it->second[1]+"}",100,0,1);
      book<TH1F>("NEF_AK8"+it->second[0]+etaBinSuffix,"neutral em fraction^{"+it->second[1]+"}",100,0,1);
    }
    book<TH1F>("qjets_volatility"+it->second[0],"qjets_volatility^{"+it->second[1]+"}",20,-2,2);
    book<TH1F>("tau1"+it->second[0],"#tau_{1}^{"+it->second[1]+"}",20,0,1.0); 
    book<TH1F>("tau2"+it->second[0],"#tau_{2}^{"+it->second[1]+"}",20,0,1.0);
    book<TH1F>("tau21"+it->second[0], "#tau_{2}/#tau_{1} all AK8^{"+it->second[1]+"}",40, 0, 1.0);
    book<TH1F>("tau3"+it->second[0],"tau3^{"+it->second[1]+"}",20,0,1);
    book<TH1F>("tau32"+it->second[0], "#tau_{3}/#tau_{2} all AK8^{"+it->second[1]+"}",40, 0, 1.0);
    book<TH1F>("tau4"+it->second[0],"tau4^{"+it->second[1]+"}",20,0,1);
    book<TH1F>("tau1_groomed"+it->second[0],"tau1_{groomed}^{"+it->second[1]+"}",20,0,1);
    book<TH1F>("tau2_groomed"+it->second[0],"tau2_{groomed}^{"+it->second[1]+"}",20,0,1);
    book<TH1F>("tau21_groomed"+it->second[0], "#tau_{2}/#tau_{1}_{groomed}^{"+it->second[1]+"}", 40, 0, 1.0);
    book<TH1F>("tau3_groomed"+it->second[0],"tau3_{groomed}^{"+it->second[1]+"}",20,0,1);
    book<TH1F>("tau32_groomed"+it->second[0], "#tau_{3}/#tau_{2}_{groomed}^{"+it->second[1]+"}", 40, 0, 1.0);
    book<TH1F>("tau4_groomed"+it->second[0],"tau4_{groomed}^{"+it->second[1]+"}",20,0,1);
    book<TH1F>("ecfN2_beta1"+it->second[0],"ecfN2_beta1^{"+it->second[1]+"}",40,-1.5,1);
    book<TH1F>("ecfN2_beta2"+it->second[0],"ecfN2_beta2^{"+it->second[1]+"}",40,-1.5,1);
    book<TH1F>("ecfN3_beta1"+it->second[0],"ecfN3_beta1^{"+it->second[1]+"}",100,-1.5,4);
    book<TH1F>("ecfN3_beta2"+it->second[0],"ecfN3_beta2^{"+it->second[1]+"}",100,-1.5,4);
    book<TH1F>("mvahiggsdiscr"+it->second[0],"mvahiggsdiscr^{"+it->second[1]+"}",20,-1.5,1.5);
  }
	
  // const int NBINS=104;
  // Double_t BOUNDARIES[NBINS] = {1, 3, 6, 10, 16, 23, 31, 40, 50, 61, 74, 88, 103, 119, 137, 156, 176, 197, 220, 244, 270, 296, 325, 354, 386, 419, 453, 489, 526, 565, 606, 649, 693, 740, 788, 838, 890, 944, 1000, 1058, 1118, 1181, 1246, 1313, 1383, 1455, 1530, 1607, 1687, 1770, 1856, 1945, 2037, 2132, 2231, 2332, 2438, 2546, 2659, 2775, 2895, 3019, 3147, 3279, 3416, 3558, 3704, 3854, 4010, 4171, 4337, 4509, 4686, 4869, 5058, 5253, 5455, 5663, 5877, 6099, 6328, 6564, 6808, 7060, 7320, 7589, 7866, 8152, 8447, 8752, 9067, 9391, 9726, 10072, 10430, 10798, 11179, 11571, 11977, 12395, 12827, 13272, 13732, 14000};
	
  // book<TH1F>("M_jj_AK8", "M_{jj-AK8} [GeV/c^{2}]",NBINS-1,BOUNDARIES);
  // book<TH1F>("M_jj_AK4", "M_{jj-AK4} [GeV/c^{2}]",NBINS-1,BOUNDARIES);
  book<TH1F>("M_jj_AK8", "M_{jj-AK8} [GeV/c^{2}]",100,0,5000);
  book<TH1F>("M_jj_AK4", "M_{jj-AK4} [GeV/c^{2}]",100,0,5000);
  book<TH1F>("detaAK4", "|#Delta#eta_{jj-AK4}|",50,0,6.5);
	

	
  //Check for Noise from Calorimeter
  book<TH1F>("met_pt_over_mjjAK8_2","MET/M_{jj-AK8}",40,0,2);
  book<TH1F>("met_pt_over_mjjAK8_4","MET/M_{jj-AK8}",40,0,4);
	
  book<TH1F>("met_pt_over_sumptAK8_2","MET/#Sigma_{AK8-Jets} p_{T}",40,0,2);
  book<TH1F>("met_pt_over_sumptAK8_4","MET/#Sigma_{AK8-Jets} p_{T}",40,0,4);

  book<TH1F>("met_pt_over_sumptAK4_2","MET/#Sigma_{AK4-Jets} p_{T}",40,0,2);
  book<TH1F>("met_pt_over_sumptAK4_4","MET/#Sigma_{AK4-Jets} p_{T}",40,0,4);


  book<TH1F>("DeltaR_ele_AK4","#Delta R (electron, any AK4)",100,0,10);
  book<TH1F>("DeltaR_AK8_AK4","#Delta R (AK8, any AK4)",100,0,10);

  book<TH1F>("DeltaR_Wlep_closestBtag","#Delta R (W_{lep},closest b-tagged AK4)",100,0,10);
  book<TH1F>("pT_Wlep","p_{T}^{W_{lep}}",100,0,1000);

  //
  //END ETABINS
  //
  book<TH1F>("N_Ele", "N_{e}", 20, 0, 20);  
  book<TH1F>("pT_Ele", "p_{T}^{e} [GeV/c]", 40 ,0 ,3000);
  book<TH1F>("eta_Ele", "#eta^{e}", 40, -6.5, 6.5);
  book<TH2F>("eta_phi_Ele", "#eta v. phi electron",100,-4.8,4.8,144,-3.15,3.15);
  book<TH1F>("E_Ele","E_{e} [GeV]",100,0,5000);
  book<TH1F>("M_Ele","M_{e} [GeV/c^{2}]",100,0,1000);


  //MET,MET/sumPt dphi(jet,MET) Eta-Phi map in high charged EM fraction (introduce leading/subleading jet CEF bins)
  book<TH1F>("met_pt_over_sumET_2","MET/#Sigma E_{T}",40,0,2);
  book<TH1F>("met_pt_over_sumET_4","MET/#Sigma E_{T}",40,0,4);
	
  book<TH1F>("met_significance","mETSignificance",80,0,4);
	
  book<TH1F>("met_sumEt","#Sigma E_{T}",90,0,9000);
	
  book<TH1F>("met_pt","Puppi MET",40,0,3000);
	
  for(uint jetRadius=0; jetRadius<=1;jetRadius++){
    for(uint iJet=0; iJet<=1; iJet++){
      std::string probeJet="";
      if(jetRadius==0){
        probeJet+="_ProbeAK4";
      }else{
        probeJet+="_ProbeAK8";				
      }
      if(iJet==0){
        probeJet+="_1";
      }else{
        probeJet+="_2";
      }

      //Inclusive
      book<TH1F>("met_pt_over_sumET_2"+probeJet,"MET/#Sigma E_{T}",40,0,2);
      book<TH1F>("met_pt_over_sumET_4"+probeJet,"MET/#Sigma E_{T}",40,0,4);
      book<TH1F>("met_significance"+probeJet,"mETSignificance",80,0,4);
      book<TH1F>("met_sumEt"+probeJet,"#Sigma E_{T}",90,0,9000);
      book<TH1F>("met_pt"+probeJet,"Puppi MET",40,0,3000);
      book<TH1F>("DeltaPhi_MET"+probeJet,"#Delta #phi(MET,"+probeJet+")",20,-6.5,6.5);		
      book<TH2F>("eta_phi"+probeJet, "#eta v. phi "+probeJet,100,-4.8,4.8,144,-3.15,3.15);
      book<TH1F>("N_electron"+probeJet,"N_{electron}",20,0,20);


      for(uint i=0; i<=cefBins.size()-2; i++){
        float cefThreshold=cefBins[i];
        std::string cefLOWSuffix =(std::string) TString::Format("_cefLT%.2f",cefThreshold).ReplaceAll(".","p");
        std::string cefHIGHSuffix =(std::string) TString::Format("_cefGT%.2f",cefThreshold).ReplaceAll(".","p");
        cefLOWSuffix=probeJet+cefLOWSuffix;
        cefHIGHSuffix=probeJet+cefHIGHSuffix;
							
        //LOW
        book<TH1F>("met_pt_over_sumET_2"+cefLOWSuffix,"MET/#Sigma E_{T}",40,0,2);
        book<TH1F>("met_pt_over_sumET_4"+cefLOWSuffix,"MET/#Sigma E_{T}",40,0,4);
        book<TH1F>("met_significance"+cefLOWSuffix,"mETSignificance",80,0,4);
        book<TH1F>("met_sumEt"+cefLOWSuffix,"#Sigma E_{T}",90,0,9000);
        book<TH1F>("met_pt"+cefLOWSuffix,"Puppi MET",40,0,3000);
        book<TH1F>("DeltaPhi_MET"+cefLOWSuffix,"#Delta #phi(MET,"+probeJet+")",20,-6.5,6.5);		
        book<TH2F>("eta_phi"+cefLOWSuffix, "#eta v. phi "+probeJet,100,-4.8,4.8,144,-3.15,3.15);

        //HIGH
        book<TH1F>("met_pt_over_sumET_2"+cefHIGHSuffix,"MET/#Sigma E_{T}",40,0,2);
        book<TH1F>("met_pt_over_sumET_4"+cefHIGHSuffix,"MET/#Sigma E_{T}",40,0,4);
        book<TH1F>("met_significance"+cefHIGHSuffix,"mETSignificance",80,0,4);
        book<TH1F>("met_sumEt"+cefHIGHSuffix,"#Sigma E_{T}",90,0,9000);
        book<TH1F>("met_pt"+cefHIGHSuffix,"Puppi MET",40,0,3000);
        book<TH1F>("DeltaPhi_MET"+cefHIGHSuffix,"#Delta #phi(MET,"+probeJet+")",20,-6.5,6.5);		
        book<TH2F>("eta_phi"+cefHIGHSuffix, "#eta v. phi "+probeJet,100,-4.8,4.8,144,-3.15,3.15);

        book<TH1F>("pT_"+cefHIGHSuffix, "p_{T}^{"+probeJet+"} [GeV/c]", 40 ,0 ,3000);
        book<TH1F>("eta_"+cefHIGHSuffix, "#eta^{"+probeJet+"}", 40, -6.5, 6.5);

        book<TH1F>("N_electron"+cefHIGHSuffix,"N_{electron}",20,0,20);
        // book<TH1F>("Event"+cefHIGHSuffix,"Event",1000000,0,1000000);
        // book<TH1F>("Run"+cefHIGHSuffix,"Run",4001,-2000,2000);
        // book<TH1F>("Lumiblock"+cefHIGHSuffix,"Lumiblock",5000,0,5000);
        // book<TH1F>("Event_raw"+cefHIGHSuffix,"Event",1000000,0,1000000);
        // book<TH1F>("Run_raw"+cefHIGHSuffix,"Run",4001,-2000,2000);
        // book<TH1F>("Lumiblock_raw"+cefHIGHSuffix,"Lumiblock",5000,0,5000);
				
      }		
    }
  }
	
	
	
	
  book<TH1F>("HT", "H_{T} [GeV/c]", 100 ,0 ,3000);
	
  h_ht = ctx.get_handle<double>("HT");

  h_ptAve = ctx.get_handle<double>("pt_ave");
	
  book<TH1F>("QScale_ratio", "#frac{#hat{p_{T}}}{p_{T}^{reco}}", 201, -10, 10);

  h_rawCleanedJets = ctx.get_handle<vector<Jet>>("rawCleanedJets");
  h_rawCleanedTopJets = ctx.get_handle<vector<TopJet>>("rawCleanedTopJets");
	
  is_mc = ctx.get("dataset_type") == "MC";    
	
}


void SubstructureValidationHists::fill(const Event & event){
  // fill the histograms. Please note the comments in the header file:
  // 'hist' is used here a lot for simplicity, but it will be rather
  // slow when you have many histograms; therefore, better
  // use histogram pointers as members as in 'UHH2/common/include/ElectronHists.h'
  
  // Don't forget to always use the weight when filling.

  double weight = event.weight;
  int Npvs = event.pvs->size();
  hist("N_pv")->Fill(Npvs, weight);

  std::vector<Jet> preJecAK4;
  if(event.is_valid(h_rawCleanedJets)) preJecAK4=event.get(h_rawCleanedJets);

  std::vector<TopJet> preJecAK8;
  if(event.is_valid(h_rawCleanedTopJets))	preJecAK8=event.get(h_rawCleanedTopJets);
	  
  std::vector<Jet>* AK4Jets = event.jets;
  std::vector<TopJet>* AK8Jets = event.topjets;

  Int_t N_AK4=AK4Jets->size();
  Int_t N_AK8=AK8Jets->size();
  Int_t N_AK4_raw=preJecAK4.size();
  Int_t N_AK8_raw=preJecAK8.size();
  hist("N_AK4")->Fill(N_AK4,weight);
  hist("N_AK8")->Fill(N_AK8,weight);

  std::vector<Jet>* AK4Jets_toFill(new std::vector<Jet> (*event.jets));
  std::vector<TopJet>* AK8Jets_toFill(new std::vector<TopJet> (*event.topjets));

  std::vector<Jet>* rawAK4Jets_toFill(new std::vector<Jet> (preJecAK4));
  std::vector<TopJet>* rawAK8Jets_toFill(new std::vector<TopJet> (preJecAK8));
	
  for(map<char,std::vector<std::string>>::iterator it = histNameSuffix.begin(); it != histNameSuffix.end(); ++it) {
    AK4Jets_toFill->clear();
    AK4Jets_toFill->reserve(event.jets->size());
    AK8Jets_toFill->clear();
    AK8Jets_toFill->reserve(event.jets->size());

    rawAK4Jets_toFill->clear();
    rawAK4Jets_toFill->reserve(preJecAK4.size());
    rawAK8Jets_toFill->clear();
    rawAK8Jets_toFill->reserve(preJecAK8.size());
    switch (it->first)
    {
    case 'a':
      for(const Jet & thisjet: *event.jets){
        AK4Jets_toFill->push_back(thisjet);
      }
      for(const TopJet & thisjet: *event.topjets){
        AK8Jets_toFill->push_back(thisjet);
      }
      for(const Jet  thisjet: preJecAK4){
        rawAK4Jets_toFill->push_back(thisjet);
      }
      for(const TopJet  thisjet: preJecAK8){
        rawAK8Jets_toFill->push_back(thisjet);
      }
      break;
    case 'f':
      if(N_AK4>=1)AK4Jets_toFill->push_back(event.jets->at(0));
      if(N_AK8>=1)AK8Jets_toFill->push_back(event.topjets->at(0));
      if(N_AK4_raw>=1)rawAK4Jets_toFill->push_back(preJecAK4.at(0));
      if(N_AK8_raw>=1)rawAK8Jets_toFill->push_back(preJecAK8.at(0));
      break;
    case 's':
      if(N_AK4>=2)AK4Jets_toFill->push_back(event.jets->at(1));
      if(N_AK8>=2)AK8Jets_toFill->push_back(event.topjets->at(1));
      if(N_AK4_raw>=2)rawAK4Jets_toFill->push_back(preJecAK4.at(1));
      if(N_AK8_raw>=2)rawAK8Jets_toFill->push_back(preJecAK8.at(1));
      break;
    case 'l':
      if(N_AK4>=2)AK4Jets_toFill->push_back(event.jets->at(0));
      if(N_AK4>=2)AK4Jets_toFill->push_back(event.jets->at(1));
      if(N_AK8>=2)AK8Jets_toFill->push_back(event.topjets->at(0));
      if(N_AK8>=2)AK8Jets_toFill->push_back(event.topjets->at(1));
      if(N_AK4_raw>=1)rawAK4Jets_toFill->push_back(preJecAK4.at(0));
      if(N_AK8_raw>=1)rawAK8Jets_toFill->push_back(preJecAK8.at(0));
      if(N_AK4_raw>=2)rawAK4Jets_toFill->push_back(preJecAK4.at(1));
      if(N_AK8_raw>=2)rawAK8Jets_toFill->push_back(preJecAK8.at(1));
      break;
    default:
      for(const Jet & thisjet: *event.jets){
        AK4Jets_toFill->push_back(thisjet);
      }
      for(const TopJet & thisjet: *event.topjets){
        AK8Jets_toFill->push_back(thisjet);
      }
      for(const Jet  thisjet: preJecAK4){
        rawAK4Jets_toFill->push_back(thisjet);
      }
      for(const TopJet  thisjet: preJecAK8){
        rawAK8Jets_toFill->push_back(thisjet);
      }
    }
		

    for(const Jet & thisjet : *AK4Jets_toFill){
      hist(("pT_AK4"+it->second[0]).c_str())->Fill(thisjet.pt(),weight);
      hist(("pT_raw_AK4"+it->second[0]).c_str())->Fill(thisjet.pt()*thisjet.JEC_factor_raw(),weight);
      hist(("eta_AK4"+it->second[0]).c_str())->Fill(thisjet.eta(),weight);
      hist(("E_AK4"+it->second[0]).c_str())->Fill(thisjet.energy());
      hist(("M_AK4"+it->second[0]).c_str())->Fill(thisjet.v4().M(),weight);
      ((TH2D*)hist(("eta_phi_AK4"+it->second[0]).c_str()))->Fill(thisjet.v4().eta(),thisjet.v4().phi(),weight);
      hist(("N_daughters_AK4"+it->second[0]).c_str())->Fill(thisjet.numberOfDaughters(),weight);
      for(uint i=0; i<=etaBins.size()-2; i++){
        float etaMin=etaBins[i];
        float etaMax=etaBins[i+1];
        bool inclusiveEta = (etaMin < 0);
        if((abs(thisjet.eta()) > etaMin && abs(thisjet.eta()) < etaMax) || inclusiveEta){
          std::string etaBinSuffix=getEtaBinSuffix(etaMin,etaMax);
          hist(("CHF_AK4"+it->second[0]+etaBinSuffix).c_str())->Fill(thisjet.chargedHadronEnergyFraction(), weight);
          hist(("NHF_AK4"+it->second[0]+etaBinSuffix).c_str())->Fill(thisjet.neutralHadronEnergyFraction(), weight);
          hist(("CEF_AK4"+it->second[0]+etaBinSuffix).c_str())->Fill(thisjet.chargedEmEnergyFraction(), weight);
          hist(("NEF_AK4"+it->second[0]+etaBinSuffix).c_str())->Fill(thisjet.neutralEmEnergyFraction(), weight);
        }
      }
    }
		
    for(const TopJet & thisjet : *AK8Jets_toFill){
      hist(("N_daughters_AK8"+it->second[0]).c_str())->Fill(thisjet.numberOfDaughters(),weight);
      hist(("pT_AK8"+it->second[0]).c_str())->Fill(thisjet.pt(),weight);
      hist(("pT_raw_AK8"+it->second[0]).c_str())->Fill(thisjet.pt()*thisjet.JEC_factor_raw(),weight);
      hist(("eta_AK8"+it->second[0]).c_str())->Fill(thisjet.eta(),weight);
      hist(("E_AK8"+it->second[0]).c_str())->Fill(thisjet.energy(),weight);
      hist(("M_AK8"+it->second[0]).c_str())->Fill(thisjet.v4().M(),weight);
      hist(("M_softdrop"+it->second[0]).c_str())->Fill(thisjet.softdropmass(),weight);
			
      for(uint i=0; i<=etaBins.size()-2; i++){
        float etaMin=etaBins[i];
        float etaMax=etaBins[i+1];
        bool inclusiveEta = (etaMin < 0);
        if((abs(thisjet.eta()) > etaMin && abs(thisjet.eta()) < etaMax) || inclusiveEta){
          std::string etaBinSuffix=getEtaBinSuffix(etaMin,etaMax);
          hist(("CHF_AK8"+it->second[0]+etaBinSuffix).c_str())->Fill(thisjet.chargedHadronEnergyFraction(), weight);
          hist(("NHF_AK8"+it->second[0]+etaBinSuffix).c_str())->Fill(thisjet.neutralHadronEnergyFraction(), weight);
          hist(("CEF_AK8"+it->second[0]+etaBinSuffix).c_str())->Fill(thisjet.chargedEmEnergyFraction(), weight);
          hist(("NEF_AK8"+it->second[0]+etaBinSuffix).c_str())->Fill(thisjet.neutralEmEnergyFraction(), weight);
        }
      }
      hist(("qjets_volatility"+it->second[0]).c_str())->Fill(thisjet.qjets_volatility(),weight);
      hist(("tau1"+it->second[0]).c_str())->Fill(thisjet.tau1(),weight);
      hist(("tau2"+it->second[0]).c_str())->Fill(thisjet.tau2(),weight);
      hist(("tau21"+it->second[0]).c_str())->Fill(thisjet.tau2()/thisjet.tau1(),weight);
      hist(("tau3"+it->second[0]).c_str())->Fill(thisjet.tau3(),weight);
      hist(("tau32"+it->second[0]).c_str())->Fill(thisjet.tau3()/thisjet.tau2(),weight);
      hist(("tau4"+it->second[0]).c_str())->Fill(thisjet.tau4(),weight);
      hist(("tau1_groomed"+it->second[0]).c_str())->Fill(thisjet.tau1_groomed(),weight);
      hist(("tau2_groomed"+it->second[0]).c_str())->Fill(thisjet.tau2_groomed(),weight);
      hist(("tau21_groomed"+it->second[0]).c_str())->Fill(thisjet.tau2_groomed()/thisjet.tau1_groomed(),weight);
      hist(("tau3_groomed"+it->second[0]).c_str())->Fill(thisjet.tau3_groomed(),weight);
      hist(("tau32_groomed"+it->second[0]).c_str())->Fill(thisjet.tau3_groomed()/thisjet.tau2_groomed(),weight);
      hist(("tau4_groomed"+it->second[0]).c_str())->Fill(thisjet.tau4_groomed(),weight);
      hist(("ecfN2_beta1"+it->second[0]).c_str())->Fill(thisjet.ecfN2_beta1(),weight);
      hist(("ecfN2_beta2"+it->second[0]).c_str())->Fill(thisjet.ecfN2_beta2(),weight);
      hist(("ecfN3_beta1"+it->second[0]).c_str())->Fill(thisjet.ecfN3_beta1(),weight);
      hist(("ecfN3_beta2"+it->second[0]).c_str())->Fill(thisjet.ecfN3_beta2(),weight);
      hist(("mvahiggsdiscr"+it->second[0]).c_str())->Fill(thisjet.mvahiggsdiscr(),weight);
    }
  }			
	

			
	
  if(N_AK8>=2)hist("M_jj_AK8")->Fill((event.topjets->at(0).v4()+event.topjets->at(1).v4()).M(),weight);
  if(N_AK4>=2){
    hist("M_jj_AK4")->Fill((event.jets->at(0).v4()+event.jets->at(1).v4()).M(),weight);
    hist("detaAK4")->Fill(abs(event.jets->at(0).eta()-event.jets->at(1).eta()),weight);
  }
	

  //Check for Noise from Calorimeter
	
  assert(event.met);

  if(N_AK8>=2){
    hist("met_pt_over_mjjAK8_2")->Fill(event.met->pt()/(AK8Jets->at(0).v4()+AK8Jets->at(1).v4()).M(),weight);
    hist("met_pt_over_mjjAK8_4")->Fill(event.met->pt()/(AK8Jets->at(0).v4()+AK8Jets->at(1).v4()).M(),weight);
  }
  float Sum_ptAK8=0.0;
  for(const TopJet & thisjet : *AK8Jets){
    Sum_ptAK8+=thisjet.pt();
  }
  if(Sum_ptAK8!=0.0){
    hist("met_pt_over_sumptAK8_2")->Fill(event.met->pt()/Sum_ptAK8,weight);
    hist("met_pt_over_sumptAK8_4")->Fill(event.met->pt()/Sum_ptAK8,weight);
  }
	
  float Sum_ptAK4=0.0;
  for(const Jet & thisjet : *AK4Jets){
    Sum_ptAK4+=thisjet.pt();
  }
  if(Sum_ptAK4!=0.0){
    hist("met_pt_over_sumptAK4_2")->Fill(event.met->pt()/Sum_ptAK4,weight);
    hist("met_pt_over_sumptAK4_4")->Fill(event.met->pt()/Sum_ptAK4,weight);
  }
		
  for(const Jet & thisjet : *AK4Jets){
    Sum_ptAK4+=thisjet.pt();
		
    if(event.electrons->size()>0){
      float detael = event.jets->at(0).eta() - event.electrons->at(0).eta();
      float dphiel = event.jets->at(0).phi() - event.electrons->at(0).phi();
      float dRel = sqrt(detael*detael+dphiel*dphiel);
      hist("DeltaR_ele_AK4")->Fill(dRel,weight);
    }
    if(N_AK8>0){
      float detaAK8 = event.jets->at(0).eta() - event.topjets->at(0).eta();
      float dphiAK8 = event.jets->at(0).phi() - event.topjets->at(0).phi();
      float dRAK8 = sqrt(detaAK8*detaAK8+dphiAK8*dphiAK8);
      hist("DeltaR_AK8_AK4")->Fill(dRAK8,weight);		
    }
  }
	
		
  if(event.electrons->size()==1){
    std::vector<LorentzVector> neutrinos=NeutrinoReconstruction(event.electrons->at(0).v4(),event.met->v4());
    float maxWlepPt=-1;
    LorentzVector WlepCand;
    for(unsigned int i=0; i<neutrinos.size();i++){
      LorentzVector Wlep=event.electrons->at(0).v4()+neutrinos[i];
      if(Wlep.pt()>maxWlepPt){
        maxWlepPt=Wlep.pt();
        WlepCand=Wlep;
      }
    }
    hist("pT_Wlep")->Fill(WlepCand.pt());
    JetId btag=JetId(BTag(BTag::CSVV2,BTag::WP_TIGHT));
    float smallestDR=10000;
		
    for(const auto & jet : *event.jets){
      if(!btag(jet,event)) continue;
      float newDR=deltaR(WlepCand,jet.v4());
      if(smallestDR>newDR) smallestDR=newDR;
    }
    hist("DeltaR_Wlep_closestBtag")->Fill(smallestDR,weight);
  }
	
  hist("met_pt_over_sumET_2")->Fill(event.met->pt()/event.met->sumEt(),weight);
  hist("met_pt_over_sumET_4")->Fill(event.met->pt()/event.met->sumEt(),weight);
  hist("met_significance")->Fill(event.met->mEtSignificance(),weight);
  hist("met_sumEt")->Fill(event.met->sumEt(),weight);
  hist("met_pt")->Fill(event.met->pt(),weight);	

  for(uint jetRadius=0; jetRadius<=1;jetRadius++){
    for(uint iJet=0; iJet<=1; iJet++){
      Jet probeJet;
      std::string probeJet_str="";
      if(jetRadius==0){
        probeJet_str+="_ProbeAK4";
        if(!(event.jets->size()>iJet))continue;
        probeJet = event.jets->at(iJet);
      }else{
        if(!(event.topjets->size()>iJet))continue;
        probeJet_str+="_ProbeAK8";				
        probeJet = event.topjets->at(iJet);
      }
      if(iJet==0){
        probeJet_str+="_1";
      }else{
        probeJet_str+="_2";
      }
      //inclusive				
      hist(("met_pt_over_sumET_2"+probeJet_str).c_str())->Fill(event.met->pt()/event.met->sumEt(),weight);
      hist(("met_pt_over_sumET_4"+probeJet_str).c_str())->Fill(event.met->pt()/event.met->sumEt(),weight);
      hist(("met_significance"+probeJet_str).c_str())->Fill(event.met->mEtSignificance(),weight);
      hist(("met_sumEt"+probeJet_str).c_str())->Fill(event.met->sumEt(),weight);
      hist(("met_pt"+probeJet_str).c_str())->Fill(event.met->pt(),weight);
      hist(("DeltaPhi_MET"+probeJet_str).c_str())->Fill(probeJet.eta()-event.met->phi(),weight);
      ((TH2D*)hist(("eta_phi"+probeJet_str).c_str()))->Fill(probeJet.eta(),probeJet.phi(),weight);
      hist(("N_electron"+probeJet_str).c_str())->Fill(event.electrons->size(),weight);


      float probeJetCEF=probeJet.chargedEmEnergyFraction();
      for(uint i=0; i<=cefBins.size()-2; i++){
        float cefThreshold=cefBins[i];
        std::string cefLOWSuffix =(std::string) TString::Format("_cefLT%.2f",cefThreshold).ReplaceAll(".","p");
        std::string cefHIGHSuffix =(std::string) TString::Format("_cefGT%.2f",cefThreshold).ReplaceAll(".","p");
        cefLOWSuffix=probeJet_str+cefLOWSuffix;
        cefHIGHSuffix=probeJet_str+cefHIGHSuffix;

        if(probeJetCEF<cefThreshold){
          //LOW				
          hist(("met_pt_over_sumET_2"+cefLOWSuffix).c_str())->Fill(event.met->pt()/event.met->sumEt(),weight);
          hist(("met_pt_over_sumET_4"+cefLOWSuffix).c_str())->Fill(event.met->pt()/event.met->sumEt(),weight);
          hist(("met_significance"+cefLOWSuffix).c_str())->Fill(event.met->mEtSignificance(),weight);
          hist(("met_sumEt"+cefLOWSuffix).c_str())->Fill(event.met->sumEt(),weight);
          hist(("met_pt"+cefLOWSuffix).c_str())->Fill(event.met->pt(),weight);
          hist(("DeltaPhi_MET"+cefLOWSuffix).c_str())->Fill(probeJet.eta()-event.met->phi(),weight);
          ((TH2D*)hist(("eta_phi"+cefLOWSuffix).c_str()))->Fill(probeJet.eta(),probeJet.phi(),weight);
        }else{
          //HIGH				
          hist(("met_pt_over_sumET_2"+cefHIGHSuffix).c_str())->Fill(event.met->pt()/event.met->sumEt(),weight);
          hist(("met_pt_over_sumET_4"+cefHIGHSuffix).c_str())->Fill(event.met->pt()/event.met->sumEt(),weight);
          hist(("met_significance"+cefHIGHSuffix).c_str())->Fill(event.met->mEtSignificance(),weight);
          hist(("met_sumEt"+cefHIGHSuffix).c_str())->Fill(event.met->sumEt(),weight);
          hist(("met_pt"+cefHIGHSuffix).c_str())->Fill(event.met->pt(),weight);
          hist(("DeltaPhi_MET"+cefHIGHSuffix).c_str())->Fill(probeJet.eta()-event.met->phi(),weight);
          ((TH2D*)hist(("eta_phi"+cefHIGHSuffix).c_str()))->Fill(probeJet.eta(),probeJet.phi(),weight);

					
          hist(("pT_"+cefHIGHSuffix).c_str())->Fill(probeJet.pt(),weight);
          hist(("eta_"+cefHIGHSuffix).c_str())->Fill(probeJet.pt(),weight);
					
          hist(("N_electron"+cefHIGHSuffix).c_str())->Fill(event.electrons->size(),weight);
          // hist(("Event"+cefHIGHSuffix).c_str())->Fill(event.event,weight);
          // hist(("Run"+cefHIGHSuffix).c_str())->Fill(event.run,weight);
          // hist(("Lumiblock"+cefHIGHSuffix).c_str())->Fill(event.luminosityBlock,weight);
          // hist(("Event_raw"+cefHIGHSuffix).c_str())->Fill(event.event,1);
          // hist(("Run_raw"+cefHIGHSuffix).c_str())->Fill(event.run,1);
          // hist(("Lumiblock_raw"+cefHIGHSuffix).c_str())->Fill(event.luminosityBlock,1);

        }
      }		
    }
  }
	
		
  Int_t N_e=event.electrons->size();
  hist("N_Ele")->Fill(N_e,weight);
	
  if(N_e>0){
    for (auto & ele : *event.electrons){
      hist("pT_Ele")->Fill(ele.pt(),weight);
      hist("eta_Ele")->Fill(ele.eta(),weight);
      ((TH2D*)hist("eta_phi_Ele"))->Fill(ele.eta(),ele.phi(),weight);
      hist("E_Ele")->Fill(ele.energy(),weight);
      hist("M_Ele")->Fill(ele.v4().M(),weight);
    }
  }
	
  if(event.is_valid(h_ht)){
    float ht = event.get(h_ht);
    hist("HT")->Fill(ht,weight);
  }

  if(is_mc && event.is_valid(h_ptAve)){
    float ratio=event.genInfo->qScale()/event.get(h_ptAve);
    hist("QScale_ratio")->Fill(ratio,weight);
  }
	
	
	
}
SubstructureValidationHists::~SubstructureValidationHists(){}



std::string getEtaBinSuffix(float etaMin,float etaMax,const char* var){
  TString etaBinSuffix="";
  if(etaMin>=0) etaBinSuffix = TString::Format("_%sBin%.1fto%.1f",var, etaMin, etaMax).ReplaceAll(".","p");
  if(etaMax>10) etaBinSuffix = TString::Format("_%sBin%.1ftoInf",var,etaMin).ReplaceAll(".","p");
  return  (std::string) etaBinSuffix;
}
