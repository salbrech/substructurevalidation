import ROOT,os,json,sys

# file = ROOT.TFile('/nfs/dust/cms/user/albrechs/UHH2/SubstructureValOutput/UL18/uhh2.AnalysisModuleRunner.MC.MC_QCD_Flat_2018_UL18_106X.root')

tree = ROOT.TChain('AnalysisTree')

tree.Add('/nfs/dust/cms/user/albrechs/UHH2/SubstructureValOutput/UL18/uhh2.AnalysisModuleRunner.MC.MC_QCD_Flat_2018_UL18_106X.root')

events={}

event_txt = open('eventList.txt','w+')
event_txt.write('#lumiBlock;event\n')
completeList_txt = open('LumiEventPtEta.txt','w+')
completeList_txt.write('#lumiBlock;event;leadingAK4_pt;leadingAK4_eta\n')

for i in range(tree.GetEntries()):
    tree.GetEntry(i)
    lumiBlock = tree.luminosityBlock
    event = tree.event
    leadingAK4 = tree.jetsAk4Puppi
    pT = leadingAK4[0].pt()
    eta = leadingAK4[0].eta()
    
    event_txt.write('%i;%i\n'%(lumiBlock,event))
    completeList_txt.write('%i;%i;%.5f;%.5f\n'%(lumiBlock,event,pT,eta))
    
    if(lumiBlock not in events):
        events.update({lumiBlock:[event]})
    else:
        events[lumiBlock].append(event)

json.dump(events,open('eventList.json','w+'))


# def getNonZeroBins(hist,printBinContent=False):
#     nonzerobins=[]
#     for i in range(hist.GetNbinsX()):
#         if(hist.GetBinContent(i)>0):
#             if(printBinContent):
#                 print hist.GetBinContent(i)
#             bincenter=int(hist.GetBinCenter(i)-0.5)
#             nonzerobins.append(bincenter)
#     return nonzerobins

# file = ROOT.TFile('/nfs/dust/cms/user/albrechs/UHH2/SubstructureValOutput/UL18/uhh2.AnalysisModuleRunner.MC.MC_QCD_Flat_2018_UL18_106X.root')

# events = file.Get('Nak8_650sel/Event_raw_ProbeAK4_1_cefGT0p60')

# events_list = getNonZeroBins(events)
# print "N_Events:",len(events_list)

# # events_list=[]
# # for i in range(events.GetNbinsX()):
# #     if(events.GetBinContent(i)>0):
# #         event=int(events.GetBinCenter(i)-0.5)
# #         events_list.append(event)

# lumis = file.Get('Nak8_650sel/Lumiblock_raw_ProbeAK4_1_cefGT0p60')
# lumi_list = getNonZeroBins(lumis)
# # lumi_list=[]
# # for i in range(lumis.GetNbinsX()):
# #     if(lumis.GetBinContent(i)>0):
# #         lumi=int(lumis.GetBinCenter(i)-0.5)
# #         lumi_list.append(event)
# # print "N_lumis:"
# print "N_lumiblocks:",len(lumi_list)        

# runs = file.Get('Nak8_650sel/Run_raw_ProbeAK4_1_cefGT0p60')
# run_list = getNonZeroBins(runs)
# print "N_runs:",len(run_list)


# File102 = ROOT.TFile("/nfs/dust/cms/user/albrechs/UHH2/SubstructureValOutput/UL18/uhh2.AnalysisModuleRunner.MC.MC_QCD_Flat_2018_UL18_102X.root")

# canv = ROOT.TCanvas("MCComp","MCComp",600,600)
# canv.SetLogy()
# cef106 = file.Get('Nak8_650sel/CEF_AK4_1')
# cef102 = File102.Get('Nak8_650sel/CEF_AK4_1')

# cef106.SetLineColor(1)
# cef102.SetLineColor(46)

# cef102.Draw("hist")
# cef106.Draw("histsame")

# norm106=cef106.Integral(cef106.FindBin(0.6),cef106.FindBin(1.0))
# norm102=cef102.Integral(cef102.FindBin(0.6),cef102.FindBin(1.0))
# print "Integral106 0.6 to 1.0: ",norm106
# print "Integral102 0.6 to 1.0: ",norm102


# halt=raw_input("..")
