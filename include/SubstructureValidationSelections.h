#pragma once

#include "UHH2/core/include/fwd.h"
#include "UHH2/core/include/Selection.h"

namespace uhh2examples {
    
/* Select events with at least two jets in which the leading two jets have deltaphi > 2.7 and the third jet pt is
 * below 20% of the average of the leading two jets, where the minimum deltaphi and
 * maximum third jet pt fraction can be changed in the constructor.
 * The jets are assumed to be sorted in pt.
 */
  class DijetSelection: public uhh2::Selection {
  public:
    DijetSelection(float dphi_min = 2.7f, float third_frac_max = 0.2f);
    virtual bool passes(const uhh2::Event & event) override;
  private:
    float dphi_min, third_frac_max;
  };


  class invMassAK8JetSelection: public uhh2::Selection {
  public:
    invMassAK8JetSelection(float invMass_min=1050.0f);
    virtual bool passes(const uhh2::Event & event) override;
  private:
    float invMass_min;
  };
  class deltaEtaAk8Selection: public uhh2::Selection {
  public:
    deltaEtaAk8Selection(float delta_max=6.5f);
    virtual bool passes(const uhh2::Event & event) override;
  private:
    float deta_max;
  };
  class EtaPhiCleanerSelection: public uhh2::Selection {
  public:
    EtaPhiCleanerSelection(TH2D* h_map_);
    virtual bool passes(const uhh2::Event &event) override;
  private:
    TH2D* h_map;
  };		

  class SingleElectronSelection: public uhh2::Selection {
  public:
    SingleElectronSelection(float pT_min=120, float eta_max=2.5, float eta_Window_min=1.442, float eta_Window_max=1.56);
    virtual bool passes(const uhh2::Event &event) override;
  private:
    float pT_min, eta_max,eta_Window_min,eta_Window_max;
  };		

  class PtAveVsQScaleSelection: public uhh2::Selection {
  public:
    PtAveVsQScaleSelection(float cutValue=1.5);
    virtual bool passes(const uhh2::Event &event) override;
  private:
    float cutValue;
  };		

  class VBFSelection: public uhh2::Selection{
  public:
    VBFSelection(float deltaEta_,float Mjj_);	 
    virtual bool passes(const uhh2::Event & event) override;
  private:
    float deltaEtaMin,mjjMin;
  };
}
