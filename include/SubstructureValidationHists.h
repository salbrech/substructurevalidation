#pragma once

#include "UHH2/core/include/Hists.h"
#include "UHH2/core/include/Event.h"

namespace uhh2examples {

/**  \brief Example class for booking and filling histograms
 * 
 * NOTE: This class uses the 'hist' method to retrieve histograms.
 * This requires a string lookup and is therefore slow if you have
 * many histograms. Therefore, it is recommended to use histogram
 * pointers as member data instead, like in 'common/include/ElectronHists.h'.
 */
  class SubstructureValidationHists: public uhh2::Hists {
  public:
    // use the same constructor arguments as Hists for forwarding:
    SubstructureValidationHists(uhh2::Context & ctx, const std::string & dirname);

    virtual void fill(const uhh2::Event & ev) override;
    virtual ~SubstructureValidationHists();
    std::map<char, std::vector<std::string> > histNameSuffix;
    std::vector<float> Selections;
    uhh2::Event::Handle<double> h_ht;
    uhh2::Event::Handle<double> h_ptAve;
    uhh2::Event::Handle<std::vector<Jet>> h_rawCleanedJets;
    uhh2::Event::Handle<std::vector<TopJet>> h_rawCleanedTopJets;
    bool is_mc;
    const std::vector<float> etaBins={-1,0,1.3,2.5,2.8,3.0,5.0,1000.0};
    const std::vector<float> cefBins={0.2,0.4,0.6,0.7,0.8,0.9};

  };

}
