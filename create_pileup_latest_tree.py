import ROOT,json
from RecoLuminosity.LumiDB import pileupParser
import sys
from math import sqrt
#quick and dirty append pythonpath to get some working version of pileupCalc.py
sys.path.append('/cvmfs/cms.cern.ch/slc7_amd64_gcc820/cms/cmssw/CMSSW_11_0_0_pre10/bin/slc7_amd64_gcc820/')
from pileupCalc import MyErf,parseInputFile

def get_N_pu(lumi_info, mini_bias_xsec, hist,Nbins):
    '''
    changed version of fillPileupHistogram to get number of PU interactions for corresponding (run),lumisection
    lumi_info:[intlumi per LS, mean interactions]
    '''
    LSintLumi = lumi_info[0]
    RMSInt = lumi_info[1] * mini_bias_xsec
    AveNumInt = lumi_info[2] * mini_bias_xsec

    Sqrt2 = sqrt(2)

    ProbFromRMS = []
    BinWidth = hist.GetBinWidth(1)

    
    if(RMSInt > 0):

        AreaLnew = -10
        AreaL = 0

        for obs in range (Nbins):
         
            left = hist.GetBinLowEdge(obs+1)
            right = left+BinWidth

            argR = (AveNumInt-right)/Sqrt2/RMSInt
            AreaR = MyErf(argR)

            if AreaLnew<-5.:
                argL = (AveNumInt-left)/Sqrt2/RMSInt
                AreaL = MyErf(argL)
            else:
                AreaL = AreaLnew
                AreaLnew = AreaR  # save R bin value for L next time

            NewProb = (AreaL-AreaR)*0.5

            ProbFromRMS.append(NewProb)
    else:
        obs = hist.FindBin(AveNumInt)
        for bin in range (Nbins):
            ProbFromRMS.append(0.0)
        if obs<Nbins+1:            
            ProbFromRMS[obs] = 1.0
        if AveNumInt < 1.0E-5:
           ProbFromRMS[obs] = 0.  # just ignore zero values
        
    if RMSInt > 0:
        totalProb = 0
        for obs in range (Nbins):
            prob = ProbFromRMS[obs]
            val = hist.GetBinCenter(obs+1)
            #print obs, val, RMSInt,coeff,expon,prob
            totalProb += prob
            hist.Fill (val, prob * LSintLumi)
            # print('val,prob*LSintLumi')
            # print(val,prob*LSintLumi)
        if 1.0-totalProb > 0.01:
            print "Significant probability density outside of your histogram"
            print "Consider using a higher value of --maxPileupBin"
            print "Mean %f, RMS %f, Integrated probability %f" % (AveNumInt,RMSInt,totalProb)
            #    hist.Fill (val, (1 - totalProb) * LSintLumi)
    else:
        hist.Fill(AveNumInt,LSintLumi)
        print('AveNumInt,LSintLumi')
        print(AveNumInt,LSintLumi)
            
pu_latest_file_name = '/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/PileUp/pileup_latest.txt'

pu_latest_json = parseInputFile(pu_latest_file_name)

n_bins = 100
max_pu_bin = 100.

pileup_hist = ROOT.TH1D ('pileup', 'pileup', n_bins, 0., max_pu_bin)

for run in pu_latest_json.keys():
    pu_ls_list = pu_latest_json[run]
    for ls in pu_ls_list:
        lumi_info = pu_ls_list[ls]
        get_N_pu(lumi_info,69200,pileup_hist,n_bins)
        
# pu_latest_json_tree_file = ROOT.TFile('pu_latest_tree.root','RECREATE')

# pu_latest_tree = ROOT.TTree('pileup_latest','pileup_latest')


