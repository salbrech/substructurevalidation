from __future__ import print_function
import ROOT
import json,sys
import gc
from UHH2.SubstructureValidation.plotter import plotter
from UHH2.SubstructureValidation.util import update_progress


plots= []
includeData=True
MET_type="PF-"
#MET_type="Puppi "
oldRange=[None,None,None,None]
plots.append(("N_pv","Event", "N^{PV}",[0,70,None,None],-1,False))
plots.append(('N_Ele','Event','N_{electrons}'))
plots.append(("met_pt_over_sumptAK8_2","Event","MET/#Sigma_{AK8-Jets} p_{T}"))
plots.append(("met_pt_over_sumptAK4_2","Event","MET/#Sigma_{AK4-Jets} p_{T}"))
jets_combinations=[('',['AK8 jets','AK4 jets']),('_1',['1st AK8 jet','1st AK4 jet']),('_2',['2nd AK8 jet','2nd AK4 jet']),('_12',['leading AK8 jets','leading AK4 jets'])]
#etaBins=[-1.0,0.0,1.3,2.5,2.8,3.0,5.0]
etaBins=[-1.0,0.0]
for i in range(len(etaBins)-1):
    if(etaBins[i]==-1.0):
        etaBinSuffix=''
    else:
        etaBinSuffix=('_etaBin%sto%s'%(etaBins[i],etaBins[i+1])).replace('.','p')
    plots.append(("N_AK4"+etaBinSuffix,"Event", "N_{AK4 jets}"))
    plots.append(("M_jj_AK4"+etaBinSuffix,"Event","M_{jj-AK4} [GeV/c^{2}]",oldRange,4))
    plots.append(("detaAK4"+etaBinSuffix,"Event","|#Delta#eta_{jj-AK4}|",oldRange,4))
    if(etaBins[i]>=2.5):
        continue
    plots.append(("N_AK8"+etaBinSuffix,"Event", "N_{AK8 jets}"))
    plots.append(("M_jj_AK8"+etaBinSuffix,"Event","M_{jj-AK8} [GeV/c^{2}]",oldRange,4))
      
for (suf,title) in jets_combinations:
    plots.append(("pT_AK4"+suf,suf, "p_{T}^{%s} [GeV/c]"%title[1]))
    plots.append(("pT_raw_AK4"+suf,suf, "p_{T}^{raw, %s} [GeV/c]"%title[1]))
    plots.append(("eta_AK4"+suf,suf, "#eta^{%s}"%title[1],oldRange,-1,False))
    plots.append(("M_AK4"+suf,suf,"M_{%s} [GeV/c^{2}]"%title[1],[0,600,None,None],4))
    plots.append(("E_AK4"+suf,suf,"E_{%s} [GeV]"%title[1],oldRange,4))
    plots.append(("N_daughters_AK4"+suf,suf,"N_{daughters}^{%s}"%title[1],oldRange,4))
    # etaBins=[-1.0,0.0,1.3,2.5,2.8,3.0,5.0]
    etaBins=[-1.0,0.0]
    for i in range(len(etaBins)-1):
        if(etaBins[i]==-1.0):
            etaBinSuffix=''
        else:
            etaBinSuffix=('_etaBin%sto%s'%(etaBins[i],etaBins[i+1])).replace('.','p')
        plots.append(("CHF_AK4"+suf+etaBinSuffix,suf,"charged hadron fraction^{%s}"%title[1],oldRange,5))
        plots.append(("NHF_AK4"+suf+etaBinSuffix,suf,"neutral hadron fraction^{%s}"%title[1],oldRange,5))
        plots.append(("CEF_AK4"+suf+etaBinSuffix,suf,"charged em fraction^{%s}"%title[1],oldRange,5))
        plots.append(("NEF_AK4"+suf+etaBinSuffix,suf,"neutral em fraction^{%s}"%title[1],oldRange,5))   
        #plots.append(("N_daughters_AK4"+suf+etaBinSuffix,suf,"N_{daughters}^{%s}"%title[1],oldRange,4))
        

    plots.append(("pT_AK8"+suf,suf, "p_{T}^{%s} [GeV/c]"%title[0]))
    plots.append(("pT_raw_AK8"+suf,suf, "p_{T}^{raw, %s} [GeV/c]"%title[0]))
    plots.append(("eta_AK8"+suf,suf, "#eta^{%s}"%title[0],oldRange,-1,False))
    plots.append(("M_AK8"+suf,suf,"M_{%s} [GeV/c^{2}]"%title[0],oldRange,4))
    plots.append(("M_softdrop"+suf,suf,"M_{softdrop,%s} [GeV/c^{2}]"%title[0],[0,400,None,None],2,False))
    plots.append(("E_AK8"+suf,suf,"E_{%s} [GeV]"%title[0],oldRange,4))
    plots.append(("N_daughters_AK8"+suf,suf,"N_{daughters}^{%s}"%title[0],oldRange,4))
    plots.append(("CHF_AK8"+suf,suf,"charged hadron fraction^{%s}"%title[0],oldRange,5))
    plots.append(("NHF_AK8"+suf,suf,"neutral hadron fraction^{%s}"%title[0],oldRange,5))
    plots.append(("CEF_AK8"+suf,suf,"charged em fraction^{%s}"%title[0],oldRange,5))
    plots.append(("NEF_AK8"+suf,suf,"neutral em fraction^{%s}"%title[0],oldRange,5))
    plots.append(("tau1"+suf,suf,"#tau_{1}^{%s}"%title[0]))
    plots.append(("tau2"+suf,suf,"#tau_{2}^{%s}"%title[0]))
    plots.append(("tau3"+suf,suf,"#tau_{3}^{%s}"%title[0]))
    plots.append(("tau4"+suf,suf,"#tau_{4}^{%s}"%title[0]))
    plots.append(("tau21"+suf,suf, "#tau_{2}/#tau_{1} %s"%title[0],oldRange))
    plots.append(("tau32"+suf,suf, "#tau_{3}/#tau_{2} %s"%title[0],oldRange,2))
    plots.append(("tau1_groomed"+suf,suf,"#tau_{1}^{%s} SD groomed"%title[0]))
    plots.append(("tau2_groomed"+suf,suf,"#tau_{2}^{%s} SD groomed"%title[0]))
    plots.append(("tau3_groomed"+suf,suf,"#tau_{3}^{%s} SD groomed"%title[0]))
    plots.append(("tau4_groomed"+suf,suf,"#tau_{4}^{%s} SD groomed"%title[0]))
    plots.append(("tau21_groomed"+suf,suf, "#tau_{2}/#tau_{1} %s SD groomed"%title[0],oldRange))
    plots.append(("tau32_groomed"+suf,suf, "#tau_{3}/#tau_{2} %s SD groomed"%title[0],oldRange,2))
    plots.append(("ecfN2_beta1"+suf,suf,"ecfN_{2}^{#beta=1} %s"%title[0]))
    plots.append(("ecfN2_beta2"+suf,suf,"ecfN_{2}^{#beta=2} %s"%title[0]))
    plots.append(("ecfN3_beta1"+suf,suf,"ecfN_{3}^{#beta=1} %s"%title[0],oldRange,4))
    plots.append(("ecfN3_beta2"+suf,suf,"ecfN_{3}^{#beta=2} %s"%title[0],oldRange,4))
    plots.append(("mvahiggsdiscr"+suf,suf,"pfBoostedDoubleSecondaryVertexAK8BJetTags %s"%title[0]))

plots.append(("met_pt_over_sumET_2","Event","MET/#Sigma E_{T}"))
plots.append(("met_pt_over_sumET_4","Event","MET/#Sigma E_{T}"))
plots.append(("met_significance","Event","mETSignificance"))
plots.append(("met_sumEt","Event","#Sigma E_{T}"))
plots.append(("met_pt","Event","%sMET"%MET_type)) 

jets_combinations=[('_1',['1st AK8 jet','1st AK4 jet']),('_2',['2nd AK8 jet','2nd AK4 jet'])]
cefBins=[0.2,0.4,0.6,0.7,0.8,0.9]


for probeJet in ['_ProbeAK4_1','_ProbeAK4_2','_ProbeAK8_1','_ProbeAK8_2']:
    plots.append(("met_pt_over_sumET_2"+probeJet,'_'+probeJet.split('_')[-1],"MET/#Sigma E_{T}"))
    plots.append(("met_pt_over_sumET_4"+probeJet,'_'+probeJet.split('_')[-1],"MET/#Sigma E_{T}"))
    plots.append(("met_significance"+probeJet,'_'+probeJet.split('_')[-1],"mETSignificance"))
    plots.append(("met_sumEt"+probeJet,'_'+probeJet.split('_')[-1],"#Sigma E_{T}"))
    plots.append(("met_pt"+probeJet,'_'+probeJet.split('_')[-1],"%sMET"%MET_type))
    plots.append(("DeltaPhi_MET"+probeJet,'_'+probeJet.split('_')[-1],"#Delta #phi(MET,"+probeJet.replace('_',' ').replace('Probe','')+")"))
    plots.append(("N_electron"+probeJet,'_'+probeJet.split('_')[-1], "N_{electrons}"))

    # for i in range(len(cefBins)-1):
    #     cefLOWSuffix=('_cefLT%.2f'%cefBins[i]).replace('.','p')
    #     cefHIGHSuffix=('_cefGT%.2f'%cefBins[i]).replace('.','p')
    #     plots.append(("met_pt_over_sumET_2"+probeJet+cefLOWSuffix,'_'+probeJet.split('_')[-1],"MET/#Sigma E_{T}"))
    #     plots.append(("met_pt_over_sumET_4"+probeJet+cefLOWSuffix,'_'+probeJet.split('_')[-1],"MET/#Sigma E_{T}"))
    #     plots.append(("met_significance"+probeJet+cefLOWSuffix,'_'+probeJet.split('_')[-1],"mETSignificance"))
    #     plots.append(("met_sumEt"+probeJet+cefLOWSuffix,'_'+probeJet.split('_')[-1],"#Sigma E_{T}"))
    #     plots.append(("met_pt"+probeJet+cefLOWSuffix,'_'+probeJet.split('_')[-1],"%sMET"%MET_type))
    #     plots.append(("DeltaPhi_MET"+probeJet+cefLOWSuffix,'_'+probeJet.split('_')[-1],"#Delta #phi(MET,"+probeJet.replace('_',' ').replace('Probe','')+")"))
    #     #plots.append(("N_electron"+probeJet+cefLOWSuffix,'_'+probeJet.split('_')[-1], "N_{e, "+probeJet.replace('_',' ').replace('Probe','')+"}"))

    #     plots.append(("met_pt_over_sumET_2"+probeJet+cefHIGHSuffix,'_'+probeJet.split('_')[-1],"MET/#Sigma E_{T}"))
    #     plots.append(("met_pt_over_sumET_4"+probeJet+cefHIGHSuffix,'_'+probeJet.split('_')[-1],"MET/#Sigma E_{T}"))
    #     plots.append(("met_significance"+probeJet+cefHIGHSuffix,'_'+probeJet.split('_')[-1],"mETSignificance"))
    #     plots.append(("met_sumEt"+probeJet+cefHIGHSuffix,'_'+probeJet.split('_')[-1],"#Sigma E_{T}"))
    #     plots.append(("met_pt"+probeJet+cefHIGHSuffix,'_'+probeJet.split('_')[-1],"%sMET"%MET_type))
    #     plots.append(("DeltaPhi_MET"+probeJet+cefHIGHSuffix,'_'+probeJet.split('_')[-1],"#Delta #phi(MET,"+probeJet.replace('_',' ').replace('Probe','')+")"))
    #     plots.append(("N_electron"+probeJet+cefHIGHSuffix,'_'+probeJet.split('_')[-1], "N_{electrons}"))
    
    
print(len(plots))

# plots = plots[15:16]
# print(plots)
cuts = sys.argv[-1:]
NPlots = len(plots)*len(cuts)
steeringName=sys.argv[-2]
config=json.load(open(steeringName))            
counter=1
gc.disable()

path = config['location']
signalKeys=list(config['samples']['signal'].keys())
bgKeys=list(config['samples']['background'].keys())
dataKeys=list(config['samples']['data'].keys())
from ROOT import TFile,gROOT
#gROOT.ProcessLine( "gErrorIgnoreLevel = 2001;")
SFiles={}
for (k,v) in config['samples']['signal'].items():
    if v['file']:
        SFiles.update({k:TFile(path+v['file'])})
        ##Open Files to get BackgroundHist:
BFiles={}
for (k,v) in config['samples']['background'].items():
    if v['file']:
        BFiles.update({k:TFile(path+v['file'])})
        
#Open File to get DataHist:
DataFiles={}
for (k,v) in config['samples']['data'].items():
    if v['file']:
        DataFiles.update({k:TFile(path+v['file'])})

BGDrawOptions={}
for key in bgKeys:
    BGDrawOptions.update({key:config['samples']['background'][key]['drawOpt']})

    
for cut in cuts:
    for plot in plots:
        hist_dir = cut+'/'+plot[0]
        print(hist_dir)
        SHists={}
        for key  in signalKeys:
            SHists.update({key:SFiles[key].Get(hist_dir)})
    
        BHists={}
        for key in bgKeys:
            BHists.update({key:BFiles[key].Get(hist_dir)})
            if('scale' in BGDrawOptions[key]):
                BHists[key].Scale(BGDrawOptions[key]['scale'])
            
        DataHists={}
        for key in dataKeys:
            DataHists.update({key:DataFiles[key].Get(hist_dir)})

        hists = {'data':DataHists,'background':BHists,'signal':SHists}
        # print(plot)
        plotter(config,cut,*plot,hists=hists)
        update_progress(counter,NPlots)
        counter+=1

config['samples']['data'] = {}
config['ratios']=[]
plotter(config,"EventHistsAfterCleaner","N_TrueInteractions","Event", "N^{TrueInteractions}",[0,55,0,0.1],-1,False)
gc.enable()
# gc.collect()
