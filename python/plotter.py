from __future__ import print_function
from array import array
import os, sys, csv, collections, numpy, math, gc,json
from ROOT import gROOT, gSystem, gStyle, gPad, TCanvas, TColor, TF1, TFile, TLegend, THStack, TGraph, TMath, kTRUE, kFALSE,TLatex, TPad, TLine,TH1F
import ROOT as rt

def magnitude(x):
    return int(math.floor(math.log10(x)))

gROOT.SetBatch(True)
gStyle.SetOptStat(0)
gStyle.SetOptFit(0)
gStyle.SetOptTitle(0)

gStyle.SetTextFont(43)

gStyle.SetTitleOffset(0.86,"X")
gStyle.SetTitleOffset(1.6,"Y")
# gStyle.SetPadLeftMargin(0.18)
gStyle.SetPadLeftMargin(0.1)
# gStyle.SetPadBottomMargin(0.15)
gStyle.SetPadBottomMargin(0.12)
gStyle.SetPadTopMargin(0.08)
# gStyle.SetPadRightMargin(0.08)
gStyle.SetPadRightMargin(0.1)
gStyle.SetMarkerSize(0.5)
gStyle.SetHistLineWidth(2)
gStyle.SetTitleSize(0.05, "XYZ")
gStyle.SetLabelSize(0.04, "XYZ")
gStyle.SetNdivisions(506, "XYZ")
gStyle.SetLegendBorderSize(0)


def plotter(config,plotdir,plot,outSubDir,xTitle,UserRange=[None,None,None,None],NmergeBins=-1,logY=True,hists=None):
    plottitle=plotdir+'_'+plot
    # lumi=59.74
    lumi=config['lumi']
    xLabelSize=18.
    yLabelSize=18.
    xTitleSize=20.
    yTitleSize=22.
    xTitleOffset=4.
    yTitleOffset=1.3
    logX=False    
    
    includeData=True
    printout=False
    Portrait=True
    cutname=False
    ratio=('ratios' in config) and  (len(config['ratios'])>0)
    do_ratio_errorHists = 'ratioError' in config and config['ratioError'].lower() == 'true'
    do_ratio_legend = ('ratio_legends' in config) and ratio

    binning='default'


    if(Portrait):
        canvX=600
        canvY=600
    else:
        canvX=900
        canvY=675
    

  
    path=config['location']
    if(outSubDir==''):
        outSubDir='AllJets'
    elif(outSubDir=='_1'):
        outSubDir='leadingJet'
    elif(outSubDir=='_2'):
        outSubDir='subleadingJet'
    elif(outSubDir=='_12'):
        outSubDir='leadingJets'
    
    outputPath=config['OutDir']+'/'+plotdir+'/'+outSubDir+'/'
    
    if(printout):
        print('InputPath:',path)
        print('OutputPath:',outputPath)
    #check if OutputPath exists - and if not create it!
    if not os.path.exists(outputPath):
        os.makedirs(outputPath)
    
    if(UserRange[2] == None or UserRange[3]== None):
        YRangeUser=False
    else:
        YRangeUser=True
    Ymin=UserRange[2]
    Ymax=UserRange[3]

    if(UserRange[0] == None or UserRange[1] == None):
        XRangeUser=False
    else:
        XRangeUser=True
    Xmin=UserRange[0]
    Xmax=UserRange[1]

    signalKeys=list(config['samples']['signal'].keys())
    bgKeys=list(config['samples']['background'].keys())
    dataKeys=list(config['samples']['data'].keys())

    stackOrder=config['stacking']
    stacking=stackOrder
    
    SignalDrawOptions={}
    for key in signalKeys:
        SignalDrawOptions.update({key:config['samples']['signal'][key]['drawOpt']})
       
    BGDrawOptions={}
    for key in bgKeys:
        BGDrawOptions.update({key:config['samples']['background'][key]['drawOpt']})

    DataDrawOptions={}
    for key in dataKeys:
        DataDrawOptions.update({key:config['samples']['data'][key]['drawOpt']})

    
    SHists={}
    BHists={}
    DataHists={}
    
    if(hists is None):
        gROOT.ProcessLine( "gErrorIgnoreLevel = 2001;")
        SFiles={}
        for (k,v) in config['samples']['signal'].items():
            if v['file']:
                SFiles.update({k:TFile(path+v['file'])})
        ##Open Files to get BackgroundHist:
        BFiles={}
        for (k,v) in config['samples']['background'].items():
            if v['file']:
                BFiles.update({k:TFile(path+v['file'])})

        #Open File to get DataHist:
        DataFiles={}
        for (k,v) in config['samples']['data'].items():
            if v['file']:
                DataFiles.update({k:TFile(path+v['file'])})

        if printout:print(SFiles,BFiles,DataFiles)
    
        for key  in signalKeys:
            SHists.update({key:SFiles[key].Get(plotdir+'/'+plot)})

        for key in bgKeys:
            BHists.update({key:BFiles[key].Get(plotdir+'/'+plot)})
            if('scale' in BGDrawOptions[key]):
                BHists[key].Scale(BGDrawOptions[key]['scale'])
            
        for key in dataKeys:
            DataHists.update({key:DataFiles[key].Get(plotdir+'/'+plot)})
    else:
        SHists = hists['signal']
        BHists = hists['background']
        DataHists = hists['data']

    # for i in range(len(bgKeys)):        
    #     if('QCD' in BFiles[bgKeys[i]].GetName() and len(dataKeys)>0):
    #         if(BHists[bgKeys[i]].Integral()>0):
    #             if(i<len(dataKeys)):
    #                 QCDscale=DataHists[dataKeys[i]].Integral()/BHists[bgKeys[i]].Integral()
    #                 QCDscale = QCDscale if QCDscale > 0 else 1.
    #             elif('scaleTo' in config['samples']['background'][bgKeys[i]]):
    #                 QCDscale=DataHists[dataKeys[config['samples']['background'][bgKeys[i]]['scaleTo']]].Integral()/BHists[bgKeys[i]].Integral()
    #             else:
    #                 QCDscale = 1.
                    
    #         else:
    #             QCDscale=1.
    #         BHists[bgKeys[i]].Scale(QCDscale)

    do_shape_norm = False
    if('shapeNorm' in config and config['shapeNorm'].lower() == 'true'):
        do_shape_norm = True
        for Hists in [SHists,BHists,DataHists]:
            for key in Hists:
                area = Hists[key].Integral()
                if(area>0):
                    Hists[key].Scale(1/area)
    
    if(binning=='dijetbinning'):
        fitbinning=array('d')
        binwidth=200
        NBins=(14000/binwidth) - ( (1040/binwidth)+1 )
        NBins=int(NBins)
        for i in range(NBins+1):
            fitbinning.append(1050+i*binwidth)
            
        for i in range(len(channels)):
            SHists[i]=SHists[i].Rebin(NBins,"new binning",fitbinning)
        for i in range(len(Backgrounds)):
            BHists[i]=BHists[i].Rebin(NBins,"new binning",fitbinning)

        if(includeData):
            DataHist=DataHist.Rebin(NBins,"new binning",fitbinning)
    if NmergeBins>0:
        for key in bgKeys:
            BHists[key].Rebin(NmergeBins,'')
        for key in signalKeys:
            SHists[key].Rebin(NmergeBins,'')
        for key in dataKeys:
            DataHists[key].Rebin(NmergeBins,'')
        
    canv = TCanvas(plottitle,plottitle,canvX,canvY)

    yplot=0.7
    yratio=0.3
    ymax=1.0
    xmax=1.0
    xmin=0.0
    if(ratio):
        plotpad=TPad("plotpad","Plot",xmin,ymax-yplot,xmax,ymax)
        ratiopad=TPad("ratiopad","Ratio",xmin,ymax-yplot-yratio,xmax,ymax-yplot)
    else:
        plotpad=TPad("plotpad","Plot",xmin,ymax-yplot-yratio,xmax,ymax)

    plotpad.SetTopMargin(0.08)
    plotpad.SetLeftMargin(0.1)
    plotpad.SetRightMargin(0.05)
    plotpad.SetTicks()
    plotpad.Draw()

    if(ratio):
        plotpad.SetBottomMargin(0.016)
        ratiopad.SetTopMargin(0.016)
        ratiopad.SetBottomMargin(0.35)
        ratiopad.SetLeftMargin(0.1)
        ratiopad.SetRightMargin(0.05)
        ratiopad.SetTicks()
        ratiopad.Draw()
    else:
        plotpad.SetBottomMargin(0.1)
        
    if(logY):
        plotpad.SetLogy()
        canv.SetLogy()
    if(logX):
        plotpad.SetLogx()
        if(ratio):
            ratiopad.SetLogx()
        canv.SetLogx()
        
    drawOptions="H"
    if(stacking):
        stack=THStack(plottitle,plottitle)

        BHist=THStack(plottitle,plottitle)

        for key in stackOrder:
            BHists[key].SetFillColor(BGDrawOptions[key]['color'])
            BHists[key].SetLineColor(BGDrawOptions[key]['color'])
            BHist.Add(BHists[key],'Hist')
        BHist.SetTitle(plottitle)

        BHistErr=BHists[bgKeys[0]].Clone()
        for i in range(1,len(bgKeys)):
            BHistErr.Add(BHists[bgKeys[i]])

        BHistErr.SetFillStyle(3204)
        BHistErr.SetFillColor(rt.kGray+2)
        BHistErr.SetLineColor(1)
        BGMax=BHist.GetMaximum()
    else:
        for key in bgKeys:
            BHists[key].SetLineColor(BGDrawOptions[key]['color'])
            BHists[key].SetLineStyle(BGDrawOptions[key]['lineStyle'])
            BHists[key].SetLineWidth(2)
            BGMax=0
            for key in bgKeys:
                tmpmax=BHists[key].GetMaximum()
                if(tmpmax>BGMax):
                    BGMax=tmpmax
            
    SIGMax=0
    for key in signalKeys:
        tmpmax=SHists[key].GetMaximum()
        if(tmpmax>SIGMax):
            SIGMax=tmpmax
    # if(scaleVV):
    #     SIGMax=SIGMax*VVScale        
    if(logY and (BGMax != 0 or SIGMax !=0)):
        MAX=0.9*float(10**(magnitude(max(BGMax,SIGMax))+4))
        MIN=float(10**(magnitude(max(BGMax,SIGMax))-5))
        MIN+=float(10**(magnitude(MIN)))
        legendMIN=math.log(max(BGMax,SIGMax))/math.log(MAX)
    else:
        MAX=1.5*max(BGMax,SIGMax)
        legendMIN=0.7
        MIN=0.
    legendMIN=(legendMIN*0.7)+0.3-0.016

    # legend_Y_bottom = 0.78 if 'legendY' not in config else config['legendY']
    # legend = TLegend(0.4,legend_Y_bottom,0.83,0.92)
    if(do_ratio_legend):
        legend = TLegend(0.50,0.78,0.9,0.92)
    else:
        legend = TLegend(0.40,0.78,0.83,0.92)
        
    legend.SetFillStyle(0)
    legend.SetTextSize(0.02)
    legend.SetMargin(0.4)
    # if(do_ratio_legend):
    #     legend.SetNColumns(2)
    legend.SetColumnSeparation(0.3)

    for key in dataKeys:
        if(DataDrawOptions[key]['markerStyle'] is not -1): DataHists[key].SetMarkerStyle(DataDrawOptions[key]['markerStyle'])
        if(DataDrawOptions[key]['lineStyle'] is not -1): DataHists[key].SetLineStyle(DataDrawOptions[key]['lineStyle'])
        if('scale' in DataDrawOptions[key]):
            DataHists[key].Scale(DataDrawOptions[key]['scale'])
        DataHists[key].SetMarkerColor(DataDrawOptions[key]['color'])
        DataHists[key].SetLineColor(DataDrawOptions[key]['color'])
        DataHists[key].SetTitle(plottitle)

    for key in signalKeys:
        SHists[key].SetLineColor(SignalDrawOptions[key]['color'])
        SHists[key].SetLineStyle(SignalDrawOptions[key]['linestyle'])
        SHists[key].SetLineWidth(2)
        legentry=SignalDrawOptions[key]['legend']
        legend.AddEntry(SHists[key],legentry)
            
    for key in bgKeys:
        legMode="f" if stacking else "l"
        legend.AddEntry(BHists[key],BGDrawOptions[key]['legend'],legMode)
        
    if(stacking):
        legend.AddEntry(BHistErr,"stat. Uncertainty","f")
        
    for key in dataKeys:
        legend.AddEntry(DataHists[key],DataDrawOptions[key]['legend'],"lep")


    canv.SetTitle(plottitle)
    if(stacking):
        if(do_shape_norm):
            raise NotImplementedError
        else:
            BHistErr.GetYaxis().SetTitle('Events')
        BHistErr.GetYaxis().SetRangeUser(MIN,MAX)
        BHistErr.GetYaxis().SetTitleFont(43)
        BHistErr.GetYaxis().SetTitleSize(yTitleSize)
        BHistErr.GetYaxis().SetTitleOffset(yTitleOffset)
        BHistErr.GetYaxis().SetLabelFont(43)
        BHistErr.GetYaxis().SetLabelSize(yLabelSize)
        if(ratio):
            BHistErr.GetXaxis().SetTitleSize(0.0)
            BHistErr.GetXaxis().SetLabelSize(0.0)
        else:
            BHistErr.GetXaxis().SetTitle(xTitle)
            BHistErr.GetXaxis().SetTitleFont(43)
            BHistErr.GetXaxis().SetTitleSize(xTitleSize)
            BHistErr.GetXaxis().SetTitleOffset(1.2)
            BHistErr.GetXaxis().SetLabelFont(43)
            BHistErr.GetXaxis().SetLabelSize(xLabelSize)

        if(YRangeUser):
            BHistErr.GetYaxis().SetRangeUser(Ymin,Ymax)
        if(XRangeUser):
            BHistErr.GetXaxis().SetRangeUser(Xmin,Xmax)

        plotpad.cd()

        BHistErr.Draw("E2")
        BHist.Draw("HistSAME")
        BHistErr.Draw("E2SAME")
    else:#do stuff here for plotting not in stack mode::
        blank=BHists[bgKeys[0]].Clone()
        blank.Reset()
        if(do_shape_norm):
            blank.GetYaxis().SetTitle('#DeltaN/N')
        else:
            blank.GetYaxis().SetTitle('Events')
        blank.GetYaxis().SetRangeUser(MIN,MAX)
        blank.GetYaxis().SetTitleFont(43)
        blank.GetYaxis().SetTitleSize(yTitleSize)
        blank.GetYaxis().SetTitleOffset(yTitleOffset)
        blank.GetYaxis().SetLabelFont(43)
        blank.GetYaxis().SetLabelSize(yLabelSize)
        if(ratio):
            blank.GetXaxis().SetTitleSize(0.0)
            blank.GetXaxis().SetLabelSize(0.0)
        else:
            blank.GetXaxis().SetTitle(xTitle)
            blank.GetXaxis().SetTitleFont(43)
            blank.GetXaxis().SetTitleSize(xTitleSize)
            blank.GetXaxis().SetTitleOffset(1.2)
            blank.GetXaxis().SetLabelFont(43)
            blank.GetXaxis().SetLabelSize(xLabelSize)

        if(YRangeUser):
            blank.GetYaxis().SetRangeUser(Ymin,Ymax)
        if(XRangeUser):
            blank.GetXaxis().SetRangeUser(Xmin,Xmax)

        plotpad.cd()

        blank.Draw("")
        for key in bgKeys:
            BHists[key].Draw("SAME"+drawOptions)

    for key in signalKeys:
        SHists[key].Draw("SAME"+drawOptions)

    for key in dataKeys:
        DataHists[key].Draw("APE1SAME")

    plotpad.RedrawAxis()

    #BEGIN RATIOPLOTS
    if(ratio):
        ratiopad.cd()

        ratioPairs=config['ratios']
        ratioHists=[]
        ratioErrorHists = []

        for ratioPair in ratioPairs:
            nominatorType=ratioPair.split(':')[0].split('/')[0]
            denominatorType=ratioPair.split(':')[0].split('/')[1]
            keys=ratioPair.split(':')[1]
            nominatorKey=keys.split('-')[0]
            denominatorKey=keys.split('-')[1]
            if('data' in nominatorType.lower()):
                newratioHist=DataHists[nominatorKey].Clone()
            elif('mc' in nominatorType.lower()):
                if(nominatorKey.lower() == 'mc'):
                    newratioHist = BHistErr.Clone()
                else:
                    newratioHist = BHists[nominatorKey].Clone()
            if('data' in denominatorType.lower()):
                newratioHist.Divide(DataHists[denominatorKey])
                ratioErrorHist = DataHists[denominatorKey].Clone()
                ratioErrorHist.Divide(DataHists[denominatorKey])
            elif('mc' in denominatorType.lower()):
                if(denominatorKey.lower() == 'mc'):
                    newratioHist.Divide(BHistErr)
                    ratioErrorHist = BHistErr.Clone()
                    ratioErrorHist.Divide(BHistErr)
                else:
                    newratioHist.Divide(BHists[denominatorKey])
                    ratioErrorHist = BHists[denominatorKey].Clone()
                    ratioErrorHist.Divide(BHists[denominatorKey])

            newratioHist.SetStats(0)
            # newratioHist.SetMarkerSize(0.7)
            newratioHist.SetLineWidth(2)

            #Yaxis
            newratioHist.GetYaxis().SetRangeUser(0.3,1.7)
            if(not do_ratio_legend):
                newratioHist.GetYaxis().SetTitle(config['ratioYTitle'])
            newratioHist.GetYaxis().CenterTitle()
            newratioHist.GetYaxis().SetTitleFont(43)
            newratioHist.GetYaxis().SetTitleSize(yTitleSize)
            newratioHist.GetYaxis().SetTitleOffset(yTitleOffset)
            newratioHist.GetYaxis().SetLabelFont(43)
            newratioHist.GetYaxis().SetLabelSize(yLabelSize)
            newratioHist.GetYaxis().SetNdivisions(506)
            #Xaxis
            newratioHist.GetXaxis().SetTitle(xTitle)
            newratioHist.GetXaxis().SetTitleFont(43)
            newratioHist.GetXaxis().SetTitleSize(xTitleSize)
            newratioHist.GetXaxis().SetTitleOffset(xTitleOffset)
            newratioHist.GetXaxis().SetLabelFont(43)
            newratioHist.GetXaxis().SetLabelSize(xLabelSize)
            newratioHist.GetXaxis().SetTickLength(0.08)
            newratioHist.GetXaxis().SetNdivisions(506)


            if(XRangeUser):
                newratioHist.GetXaxis().SetRangeUser(Xmin,Xmax)
                ratioXMin=Xmin
                ratioXMax=Xmax
            else:
                ratioXMin=newratioHist.GetXaxis().GetXmin()
                ratioXMax=newratioHist.GetXaxis().GetXmax()
            ratioHists.append(newratioHist)
            ratioErrorHists.append(ratioErrorHist)

        ratioDrawOptions=config['ratioDrawOptions']
        if(do_ratio_legend):
            ratio_legend = TLegend(0.10,0.78,0.38,0.92)
            ratio_legend.SetFillStyle(0)
            ratio_legend.SetTextSize(0.02)
            ratio_legend.SetMargin(0.4)

            ratio_legend.AddEntry(None,"","")
            ratio_legend.AddEntry(None,"Ratios:","")
        for k in range(len(ratioHists)):
            ratioHists[k].SetLineColor(ratioDrawOptions[k]['color'])
            if('lineStyle' in ratioDrawOptions[k]):
                ratioHists[k].SetLineStyle(ratioDrawOptions[k]['lineStyle'])
            ratioHists[k].SetMarkerColor(ratioDrawOptions[k]['color'])
            ratioHists[k].SetMarkerStyle(ratioDrawOptions[k]['markerStyle'])
            if(do_ratio_legend):
                ratio_legend.AddEntry(ratioHists[k],config['ratio_legends'][k],'lep')
            ratiodrawOpt="ep"
            if k>0:
                ratiodrawOpt+="SAME"
            ratioHists[k].Draw(ratiodrawOpt)
        if(do_ratio_errorHists):
            for k in range(len(ratioHists)):
                if('errcolor' in ratioDrawOptions[k]):
                    errColor = ratioDrawOptions[k]['errcolor']
                else:
                    errColor = ratioDrawOptions[k]['color']
                ratioErrorHists[k].SetFillColor(errColor)
                ratioErrorHists[k].SetMarkerStyle(1)
                # ratioErrorHists[k]self.SetFillStyle(3204)
                ratioErrorHists[k].SetFillStyle(3000+(2+k)*100+4)
                ratioErrorHists[k].Draw('E2SAME')
            for k in range(len(ratioHists)):
                ratioHists[k].Draw('EPSAME')
        #END RATIOPLOTS



        zeropercent=TLine(ratioXMin,1,ratioXMax,1)
        zeropercent.Draw()
        plus10percent=TLine(ratioXMin,1.1,ratioXMax,1.1)
        plus10percent.SetLineStyle(rt.kDashed)
        plus10percent.Draw()
        minus10percent=TLine(ratioXMin,0.9,ratioXMax,0.9)
        minus10percent.SetLineStyle(rt.kDashed)
        minus10percent.Draw()

    canv.cd()
    gPad.RedrawAxis()
    legend.Draw()
    if(do_ratio_legend):
        ratio_legend.Draw('SAME')

    latex=TLatex()
    latex.SetNDC(kTRUE)
    latex.SetTextSize(20)
    if(lumi>0):
        latex.DrawLatex(0.69,0.953,"%.2f fb^{-1} (13 TeV)"%lumi)
    else:
        latex.DrawLatex(0.82,0.953,"(13 TeV)")
    if 'etaBin' in plot:
        minEta=plot.split('etaBin')[-1].split('to')[0].replace('p','.')
        maxEta=plot.split('etaBin')[-1].split('to')[1].replace('p','.')
        binText='%.1f < |#eta| < %.1f'%(float(minEta),float(maxEta))
        if maxEta == 'inf':
            binText='|#eta| > %.1f'%(float(minEta))
        #print(binText)
        latex.DrawLatex(0.1,0.953,binText)
    if 'cefGT' in plot or 'cefLT' in plot:
        if('cefGT' in plot):
            cefSuffix=' #geq '+plot.split('cefGT')[-1].replace('p','.')
        if('cefLT' in plot):
            cefSuffix=' < '+plot.split('cefLT')[-1].replace('p','.')
        probeJet_tmp=plot.split('Probe')[-1].split('cef')[0]
        probeJet = 'leading ' if '_1_' in probeJet_tmp else 'subleading '
        probeJet += 'AK4 jet' if 'AK4' in probeJet_tmp else 'AK8 jet'
        binText = probeJet + ' with CEF'+cefSuffix
        latex.DrawLatex(0.1,0.953,binText)
    #latex.DrawLatex(0.1,0.953,"private work")

    canv.Update()
    for fileType in config["SaveAs"].split(','):
        canv.Print(outputPath+'/%s_%s.%s'%(plotdir,plot,fileType))
    #prevents memory leak in Canvas Creation/Deletion
    #see: https://root.cern.ch/root/roottalk/roottalk04/2484.html
    gSystem.ProcessEvents()
    # if(ratio):
    #     del ratiopad

    # del plotpad,canv

    # if(hists is None):
    #     for key in signalKeys:
    #         del SHists[key]
    #         SFiles[key].Close()
    #     for key in bgKeys:
    #         del BHists[key]
    #         BFiles[key].Close()
    #     for key in dataKeys:
    #         del DataHists[key]
    #         DataFiles[key].Close()
    
    # gc.collect()
    return 'done!'
if(__name__=='__main__'):

    config=json.load(open(sys.argv[1]))
    Samples=config['samples']
    RootFileDir=config['location']
    plotdir=sys.argv[2]            
    plot=sys.argv[3]
    xTitle=sys.argv[4]
    
    if(False):
        print('|RootFile Directory:', RootFileDir)
        print('|Samples:')
        for (k,v) in Samples.items():
            print("|")
            print("|"+"-"*2+k+':')
            for (k1,v1) in v.items():
                print("|"+"-"*3+k1+":")
                print("|"+"-"*6+v1['file'])
        print("|\n|")
        print("|Selections: "+plotdir )
        print("|Plot: "+plot)
    # plotter(config,plotdir, plot, xTitle)
